package exams.first;

import java.util.Scanner;

public class Question2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner input = new Scanner (System.in);  //Creating scanner for user input
		
		System.out.print("Enter an integer: ");  //Prompting user for input
		
		try {
		
		int num = Integer.parseInt(input.nextLine());  //Reading in the integer
		
		if (num % 2 == 0 && num % 3 == 0) {  //Prints out foobar if the int is divisible by both 2 and 3
			
			System.out.println("foobar");  
		}
		
		else if (num % 2 == 0 && num % 3 != 0) {  //Prints out foo if the int is divisible by 2 and not by 3
			
			System.out.println("foo");
		}
		
		else if (num % 2 != 0 && num % 3 == 0) {  //Prints out bar if the int is divisible by 3 and not by 2
			
			System.out.println("bar");
		}
		
		else {  //Prints nothing if the int isn't divisible by 2 or 3
			
			
		}
		
	}
		
		catch (Exception e) {  //Handling exceptions
			
			System.out.println("Invalid input!");
			
			System.out.println(e.getMessage());
		}
		
		input.close(); //closing the scanner
		
	}

}
