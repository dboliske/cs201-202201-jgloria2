package exams.first;

import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in); // Creating scanner for user input

		System.out.print("Enter an integer: "); // Prompting user for input
		
		try {

		int num = Integer.parseInt(input.nextLine()); // Reading in the integer

		for (int i = 0; i < num; i++) {  //Outer loop which controls the row

			for (int j = 0; j < i; j++) {  //inner loop which prints spaces. For each subsequent row, the amount of spaces printed increases by one

				System.out.print("  ");
			}
			
			for (int j = i; j < num; j++) {  //inner loop which prints asterisks. For each subsequent row, the amount of asterisks printed decreases by one
				
				System.out.print("* ");
			}

			System.out.println();  //Print statement to go to the next row

		}
		
		
		}catch (Exception e) {  //Handling Exceptions
			
			System.out.println("Invalid input!");
			
			System.out.println(e.getMessage());
		}
		
		

		input.close(); // closing scanner

	}

}
