package exams.first;

public class PetClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//This is my application class for the Pet class. Below I am testing my Pet class to ensure all of my methods work
		
		Pet p1 = new Pet();
		
		System.out.println(p1.toString());
		
		Pet p2 = new Pet("Dog" , 3);
		
		//Pet p2 = new Pet("Cat" , 1);
		
		//Pet p2 = new Pet("Dog" , 1);
		
		//Pet p2 = new Pet("Cat" , 3);
		
		//Pet p2 = new Pet("Cat" , -3);
		
		System.out.println(p2.toString());
		
		System.out.println(p1.equals(p2));
		
	}

}
