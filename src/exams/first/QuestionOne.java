package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner (System.in);  //Creating scanner for user input
		
		System.out.print("Enter an integer: ");  //Prompting user for input
		
		try {
		
		int num = Integer.parseInt(input.nextLine());  //Reading in the integer
		
		int result = num + 65;  //Adding 65 to the original integer
		
		char out = (char)result;  //Type casting the integer to a char
		
		
		System.out.println("Your resulting character is: " + out);  //Printing out the resulting char
		
		}
		
		catch (Exception e) {  //Handling exceptions
			
			System.out.println("Invalid input!");
			
			System.out.println(e.getMessage());
		}
		
		input.close();  //closing scanner
		
	}

}
