package exams.first;

public class Pet {

	private String name;  //Instance variable for name
	
	private int age;  //Instance variable for age
	
	public Pet() {  //Default constructor
		
		name = "Cat";
		
		age = 1;
	}
	
	public Pet(String name, int age ) {  //Non-default constructor
		
		this.name = name;
		
		setAge(age);  //Validation using setAge to ensure age is positive
	}
	
	
	public void setName(String name) {  //Mutator method for name
		
		this.name = name;
	}
	
	public void setAge(int age) {  //Mutator method for age. Ensures that the age is positive before being set
		
		if (age > 0) {
			
			this.age = age;
		}
		
		else {
			
			this.age = 1;
		}
	}
	
	public String getName() {  //Accessor method for name
		
		return name;
	}
	
	
	public int getAge() {  //Accessor method for age
		
		return age;
	}
	
	public boolean equals(Pet p) {  //equals method
		
		if (!this.name.equalsIgnoreCase(p.getName())) {
			
			return false;
		}
		
		else if (this.age != p.getAge()) { 
			
			return false;
		}
		
		else {
			
			return true;
		}
		
	}
	
	public String toString() {  //toString method
		
		return name + " is " + age + " years old";
	}
	
	
	
	
	
}
