package exams.first;

import java.util.Scanner;

public class Question4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in); // Creating scanner for user input

		String[] word = new String[5]; //Declaring and allocating array

		for (int i = 0; i < 5; i++) {

			System.out.print("Enter a word: "); // Prompting user for input

			word[i] = input.nextLine(); // Reading in the word and storing it in array

		}
		
		int matchCount = 0;   //Declaring variable to keep track of the amount of matches
		
		for (int i = 0; i < word.length; i++) {  //Outer loop which controls the element that is being compared to the rest of the elements in the array
			
			for (int j = i; j < word.length; j++) {  //Inner loop which runs through each element starting at i and compares it to the element corresponding to the outer loop
				
				if (j != i && word[j].equals(word[i])) {
					
					
					matchCount++;  //Increasing match count if a match is found
					
					word[j] = "";  //Replacing the match with "" since it is already accounted for, so I no longer need to check if it has a match
				}
				
				
				
			}
			
			if (matchCount > 0 && word[i] != "") {  
				
				System.out.println(word[i]);  //Printing out the match
				
				matchCount = 0;  //reseting the match count
			}
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		input.close();  //closing scanner
		
		
	}

}
