package exams.second;

public abstract class Polygon {

	protected String name;  //Instance variable for name
	
	public Polygon() {  //default constructor
		
		name = "Triangle";
	}

	public String getName() {  //accessor method for name
		return name;
	}

	public void setName(String name) {  //mutator method for name
		this.name = name;
	}
	
	public String toString() {  //toString method
		
		return "Name: " + name;
	}
	
	public abstract double area();  //abstract method for area
	
	public abstract double perimeter();  //abstract method for perimeter
	
	
}
