package exams.second;

public class Classroom {
	
	protected String building;  //Instance variable for building
	protected String roomNumber;  //Instance variable for room number
	private int seats;  //Instance variable for seats
	
	
	public Classroom() {  //default constructor
		
		building = "Kaplin";
		roomNumber = "212";
		seats = 10;
	}


	public String getBuilding() {  //accessor method for building
		return building;
	}


	public void setBuilding(String building) {  //mutator method for building
		this.building = building;
	}


	public String getRoomNumber() {  //accessor method for room number
		return roomNumber;
	}


	public void setRoomNumber(String roomNumber) {  //mutator method for seats
		this.roomNumber = roomNumber;
	}


	public int getSeats() {  //accessor method for seats
		return seats;
	}


	public void setSeats(int seats) {  //mutator method for seats
		
		if (seats > 0) {
			
		this.seats = seats;
		} else {
			
			this.seats = 1;
		}
	}
	
	
	
	public String toString() {  //toString method
		
		return "Building: " + building + "; Room Number: " + roomNumber + "; Seats: " + seats + ";";
	}
	
	
	
	

}
