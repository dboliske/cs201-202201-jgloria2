package exams.second;

public class Rectangle extends Polygon {
	
	private double width;  //Instance variable for width
	private double height;  //instance variable for height
	
	public Rectangle() {  //default constructor
		
		setName("Rectangle");
		width = 1.0;
		height = 1.0;
	}
	
	
	

	public double getWidth() {  //accessor method for width
		return width;
	}




	public void setWidth(double width) {  //mutator method for width
		
		if (width > 0) {
			
		this.width = width;
		}
		
		else {
			
			this.width = 1.0;
		}
	}




	public double getHeight() {  //accessor method for height
		return height;
	}




	public void setHeight(double height) {  //mutator method for height
		
		if (height > 0) {
			
		this.height = height;
		} else {
			
			this.height = 1.0;
		}
	}


	public String toString() {  //toString method
		
		return super.toString() + "; Width: " + width + "; Height: " + height + ";";
	}


	@Override
	public double area() {  //area method
		
		double area = height * width;
		
		return area;
	}

	@Override
	public double perimeter() {  //perimeter method

		double perimeter = 2.0 * (height + width);
		
		return perimeter;
	}

}
