package exams.second;

public class PolygonApp {
	
	//*******************************************************************************************//
	//This is just an app class to test the polygon, rectangle, and circle classes. Please ignore.
	//*******************************************************************************************//

	public static void main(String[] args) {
		
		Rectangle r1 = new Rectangle();
		
		System.out.println(r1.toString());
		
		r1.setHeight(5);
		r1.setWidth(-10);
		
		System.out.println(r1.toString());
		System.out.println(r1.area());
		System.out.println(r1.perimeter());
		
		Circle c1 = new Circle();
		
		System.out.println(c1.toString());
		
		c1.setRadius(3);
		
		System.out.println(c1.toString());
		System.out.println(c1.area());
		System.out.println(c1.perimeter());

	}

}
