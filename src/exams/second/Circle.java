package exams.second;

public class Circle extends Polygon {

	private double radius;  //instance variable for radius
	
	public Circle() {  //default constructor
		
		setName("Circle");
		radius = 1.0;
	}	
	
	
	public double getRadius() {  //accessor method for radius
		return radius;
	}


	public void setRadius(double radius) {  //mutator method for radius
		
		if (radius > 0) {
			
		this.radius = radius;
		
		}else {
			
			this.radius = 1.0;
		}
	}
	
	
	public String toString() {  //toString method
		
		return super.toString() + "; Radius: " + radius + ";";
	}
	

	@Override
	public double area() {  //area method

		double area = Math.PI * radius * radius;
		
		return area;
	}

	@Override
	public double perimeter() {  //perimeter method

		double perimeter = 2.0 * Math.PI * radius;
		
		return perimeter;
	}

}
