package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLists {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		ArrayList<Double> numbers = new ArrayList<Double>();
		
		boolean running = true;
		
		do {
			
			System.out.print("Please enter a number (enter 'done' when finished): ");
			
			String number = input.nextLine();
			
			if (number.equalsIgnoreCase("done")) {
				
				running = false;
			} else {
			
			try { 
				
				double num = Double.parseDouble(number);
				
				numbers.add(num);
				
			} catch (Exception e) {
				
				System.out.println("'" + number + "' is not a valid number");
			}
			
			}
			
		} while (running);
		
		double max = numbers.get(0);
		double min = numbers.get(0);
		
		for (int i = 0; i < numbers.size(); i++) {
			
			if (numbers.get(i) > max) {
				
				max = numbers.get(i);
			}
			
			if (numbers.get(i) < min) {
				
				min = numbers.get(i);
			}
			
		}
		
		
		System.out.println("Maximum: " + max + "; Minimum: " + min + ";");
		
		
		input.close();

	}

}
