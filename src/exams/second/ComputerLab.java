package exams.second;

public class ComputerLab extends Classroom {
	
	private boolean computers;  //Instance variable for computers
	
	
	public ComputerLab() {  //default constructor
		
		super();
		computers = false;
	}


	public boolean hasComputers() {  //accessor method for computers
		return computers;
	}


	public void setComputers(boolean computers) {  //mutator method for computers
		this.computers = computers;
	}
	
	public String toString() {  //toString method
		
		return super.toString() + "Computers: " + computers;
	}
	
	
	
	
	
	

}
