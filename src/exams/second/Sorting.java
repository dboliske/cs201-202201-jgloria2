package exams.second;

public class Sorting {

	public static void main(String[] args) {

		String[] values = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		values = sort(values);
		
		for(int i = 0; i < values.length - 1; i++) {
			
			System.out.print(values[i] + ", ");
		}
		
		System.out.print(values[values.length - 1]);

	}
	
	
	public static String[] sort(String[] values) {  //selection sort method
		
		for (int i = 0; i < values.length - 1; i++) {
			
			int min = i;
			
			
			for(int j = i + 1; j < values.length; j++) {
				
				if (values[j].compareTo(values[min]) < 0) {
					
					min = j;

				}
			}
			
			if(min != i) {
				
				String temp = values[i];
				values[i] = values[min];
				values[min] = temp;
				
			}
		}
		
		return values;
	}	

}
