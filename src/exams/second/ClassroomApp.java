package exams.second;

public class ClassroomApp {

	//***************************************************************************//
	//This is just an app class to test the classroom and computer lab apps. Please ignore.
	//**************************************************************************//
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Classroom c1 = new Classroom();
		
		System.out.println(c1.toString());
		
		c1.setSeats(-6);
		
		System.out.println(c1.toString());
		
		ComputerLab cp1 = new ComputerLab();
		
		System.out.println(cp1.toString());
		
		cp1.setSeats(-1);
		
		System.out.println(cp1.toString());

	}

}
