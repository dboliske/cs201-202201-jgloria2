package exams.second;

import java.util.Scanner;

public class Searching {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		System.out.print("Search item: ");
		
		String in = input.nextLine();
		
		double num = 0;
		
		int index = -1;
		
		try {
			
			num = Double.parseDouble(in);
			
			index = search(numbers, num);
			
			if (index == -1) {
				
				System.out.println("'" + num + "' not found");
				
			} else {
			
				System.out.println("'" + num + "' is located at index " + index);
			
			}
			
		} catch (Exception e) {
			
			System.out.println("'" + in + "' is not a valid number");
			
			index = -1;
		}
		
		input.close();
		

	}
	
	
	public static int search(double[] numbers, double num) {  //utility function
		
		return search(numbers, num, (int)Math.sqrt(numbers.length), 0); 
		
	}
	
	
	public static int search(double[] numbers, double num, int step, int prev) {  //recursive jump search method
		
		
		int index = -1;
		
		if (numbers[Math.min(prev, numbers.length - 1)] == num) {  //base case
			
			index =  prev;
		} else if (numbers[Math.min(step, numbers.length - 1)] == num) {  //base case
			
			index =  step;
		}
		
		else if (numbers[Math.min(step, numbers.length - 1)] < num) {  //general case for when the step is less than the number
			prev = step;
			step += (int)Math.sqrt(numbers.length);
			
			  if (prev >= numbers.length) { 
				  
				  return -1; 
				  }
			 
			
			index = search(numbers, num, step, prev);
		}
		
		else if (numbers[prev] < num) {  //general case for when the previous step is less than the number
			prev++;
			
			  if (prev == Math.min(step, numbers.length - 1)) { 
				  
				  return -1;
				  }
			 
			
			index = search(numbers, num, step, prev);
		}
		
		
		return index;
	}

}
