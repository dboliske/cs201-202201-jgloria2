package practice.exam2;

public class Abstract1App {

	public static void main(String[] args) {


		Abstract1Cat cat1 = new Abstract1Cat("socks");
		Abstract1Dog dog1 = new Abstract1Dog();
		
		System.out.println(cat1);
		System.out.println(dog1);
		
		cat1.speak();
		dog1.speak();
	}

}
