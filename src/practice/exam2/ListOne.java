package practice.exam2;

import java.util.ArrayList;
import java.util.Scanner;

public class ListOne {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		ArrayList<Double> list = list(input);
		
		for (int i = 0; i < list.size() - 1; i++) {
			
			System.out.print(list.get(i) + ",");
		}
		
		System.out.print(list.get(list.size() - 1) + "");
		
		input.close();
	}
	
	
	public static ArrayList<Double> list(Scanner input) {
		
		ArrayList<Double> numbers = new ArrayList<Double>(25);
		
		double num = 0;
		String in = null;
		for (int i = 0; i < 25; i++) {
			
		System.out.print("Enter a number: ");
		try {
		in = input.nextLine();
		num = Double.parseDouble(in);
		numbers.add(num);
		} catch (Exception e) {
			
			System.out.println("'" + in + "' is not a valid number.");
		}
		}
		
		return numbers;

	}
	
}
