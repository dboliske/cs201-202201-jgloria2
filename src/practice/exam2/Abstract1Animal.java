package practice.exam2;

public abstract class Abstract1Animal {  //Remember to make this an abstract class since it contains an abstract method

	protected String name;

	public Abstract1Animal() {
		
		name = "Fluffy";
	}
	
	public Abstract1Animal(String name) {
		
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		
		return "Animal's name :" + name;
	}
	
	public boolean equals(Object obj) {
		
		if (obj == null) {
			
			return false;
		} else if (this == obj) {
		
			return true;
		} else if (!(obj instanceof Abstract1Animal)) {
			
			return false;
		}
		
		Abstract1Animal animal = (Abstract1Animal)obj;
		
		if (!name.equals(animal.getName())) {
			
			return false;
		}
		
		return true;
	}
	
	/*
	 * public void speak() {  //Rather than having a method with empty braces, just make the method abstract
	 * 
	 * 
	 * }
	 */
	
	public abstract void speak();
	
}
