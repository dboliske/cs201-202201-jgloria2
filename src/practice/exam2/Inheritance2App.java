package practice.exam2;

public class Inheritance2App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Inheritance2Cube cube1 = new Inheritance2Cube();
		Inheritance2Cube cube2 = new Inheritance2Cube(2);
		
		System.out.println(cube1.volume());
		System.out.println(cube2.volume());
	}

}
