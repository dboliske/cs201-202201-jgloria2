package practice.exam2;

public class Abstract1Cat extends Abstract1Animal {

	public Abstract1Cat() {
		
		super();
	}
	
	public Abstract1Cat(String name) {
		
		super(name);
	}
	
	public String toString() {
		
		return "Cat's name: " + name;
	}
	
	public boolean equals(Object obj) {
		
		return super.equals(obj) && obj instanceof Abstract1Cat;
	}
	
	public void speak() {
		
		System.out.println("Meow");
	}
	
}
