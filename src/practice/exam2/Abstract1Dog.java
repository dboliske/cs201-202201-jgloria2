package practice.exam2;

public class Abstract1Dog extends Abstract1Animal {

	
	public Abstract1Dog() {
		
		super();
	}
	
	public Abstract1Dog(String name) {
		
		super(name);
	}
	
	public String toString() {
		
		return "Dog's name " + name;
	}
	
	public boolean equals(Object obj) {
		
		return super.equals(obj) && obj instanceof Abstract1Dog;
	}
	
	
	public void speak() {
		
		System.out.println("Woof");
	}
}
