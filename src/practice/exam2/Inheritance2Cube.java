package practice.exam2;

public class Inheritance2Cube extends Inheritance2Rectangle {

	private double depth;
	
	public Inheritance2Cube() {
		
		super();
		depth = 1;
	}
	
	public Inheritance2Cube(double side) {
		
		super(side,side);
		setDepth(side);
	}

	public double getDepth() {
		return depth;
	}

	public void setDepth(double depth) {
		
		if (depth > 0.0) {
			
		this.depth = depth;
		}
	}
	
	public String toString() {
		
		return "(" + getHeight() + ", " + getWidth() + ", " + depth + ")";
	}
	
	public boolean equals(Object obj) {
		
		super.equals(obj);
		
		if (!(obj instanceof Inheritance2Cube)) {
			
			return false;
		}
		
		Inheritance2Cube cube = (Inheritance2Cube)obj;
		
		if ((Math.abs(cube.getDepth() - this.depth) > 0.001)) {
			
			return false;
		}
		 
		
		return true;
	}
	
	public double volume() {
		
		double volume = getHeight() * getWidth() * depth;
		return volume;
	}
	
}
