package practice.exam2;

public class Inheritance2Rectangle {

	private double height;
	private double width;
	
	public Inheritance2Rectangle() {
		
		height = 1.0;
		width = 1.0;
	}
	
	public Inheritance2Rectangle(double h, double w) {
		
		this();
		setHeight(h);
		setWidth(w);
	}
	
	public double getHeight() {
		return height;
	}
	
	public void setHeight(double height) {
		
		if (height > 0.0) {
		this.height = height;
		}
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(double width) {
		if (width > 0.0) {
		this.width = width;
		}
	}
	
	public String toString() {
		
		return "(" + height + ", " + width + ")";
	}
	
	public boolean equals(Object obj) {
		
		if (obj == null) {
			
			return false;
		} else if (this == obj) {
			
			return true;
		} else if (!(obj instanceof Inheritance1Rectangle)) {
			
			return false;
		}
		
		Inheritance1Rectangle rect = (Inheritance1Rectangle)obj;
		
		if (Math.abs(height - rect.getHeight()) > 0.001) {
			
			return false;
		} else if(Math.abs(width - rect.getWidth()) > 0.001) {
			
			return false;
		}
		
		return true;
	}
	
	
	public double area() {
		
		double area = width * height;
		return area;
	}
	
	public double perimeter() {
		
		double perimeter = (2 * height) + (2 * width);
		return perimeter;
	}
	
}
