package practice.exam2;

public class Inheritance1App {

	public static void main(String[] args) {
		
		Inheritance1Rectangle rect1 = new Inheritance1Rectangle(10,5);
		
		System.out.println(rect1.area());
		System.out.println(rect1.perimeter());
		
		Inheritance1Square sqr1 = new Inheritance1Square(7);
		Inheritance1Square sqr2 = new Inheritance1Square();
		
		
		System.out.println(sqr1.getSide());
		System.out.println(sqr2.getSide());


	}

}
