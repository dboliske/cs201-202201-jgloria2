package practice.exam2;

public class Inheritance1Square extends Inheritance1Rectangle {

	public Inheritance1Square() {

		super();
	}

	/*
	 * public Inheritance1Square(double height, double width) {    //THIS IS WRONG
	 * 
	 * 
	 * 
	 * if (height != width) {
	 * 
	 * height = 1; width = 1; }
	 * 
	 * 
	 * super(height, width);
	 * 
	 * 
	 * }
	 */
	
	
	public Inheritance1Square(double side) {
		
		setHeight(side);
		setWidth(side);
	}
	
	public void setSide(double side) {
		
		setHeight(side);
		setWidth(side);
	}
	
	public String toString() {
		
		return super.toString();
	}
	
	public boolean equals(Object obj) {
		
		return super.equals(obj);
	}

	public double getSide() {

		double side = this.getHeight();

		return side;
	}

}
