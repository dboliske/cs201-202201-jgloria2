package practice.exam2;

public abstract class Abstract2Phone {

	protected String number;
	
	
	public Abstract2Phone() {
		
		number = "9227236";
	}
	
	public Abstract2Phone(String num) {
		
		setNumber(num);
	}
	
	public void setNumber(String num) {
		
		if (num.length() == 7) {
			
		number = num;
		}
	}
	
	
	public String getNumber() {
		
		return number;
	}
	
	
	public String toString() {
		
		return "Phone: " + number;
	}
	
	
	public boolean equals(Object obj) {
		
		if (obj == null) {
			
			return false;
		} else if (this == obj) {
			
			return true;
		} else if (!(obj instanceof Abstract2Phone)) {
			
			return false;
		}
		
		Abstract2Phone phone = (Abstract2Phone)obj;
		
		if (!this.number.equals(phone.getNumber())) {
			
			return false;
		}
		
		return true;
	}
	
	public abstract void call(Abstract2Phone phone);
	
	
	
	
}
