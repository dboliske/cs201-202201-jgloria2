package practice.exam2;

import java.util.Scanner;

public class BinarySearch {

	
	public static int binarySearch(String[] values, String item) {
		
		int start = 0;
		
		//int end = values.length - 1;  
		
		int end = values.length;  //Isn't the index at values.length out of bounds? Why isn't it length - 1?
		int pos = -1;
		boolean found = false;
		
		while(!found && start != end) {
			
			int mid = (start + end) / 2;
			
			if (values[mid].equalsIgnoreCase(item)) {
				
				found = true;
				pos = mid;
			}else if (values[mid].compareToIgnoreCase(item) < 0){
				
				start = mid + 1;
				
			}else {
				
				end = mid;
				
			}
		}
		
		return pos;
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		String[] values = {"c", "html", "java", "python", "ruby", "scala"};
		
		System.out.print("Search item: ");
		
		String item = input.nextLine();
		
		int index = binarySearch(values, item);
		
		if (index == -1) {
			
			System.out.println("'" + item + "' not found");
			
		} else {
		
			System.out.println("'" + item + "' is located at index " + index);
		
		}
		input.close();

	}

}
