package practice.exam2;

public class Abstract2HomePhone extends Abstract2Phone {

	public Abstract2HomePhone() {
		
		super();
	}
	
	public Abstract2HomePhone(String number) {
		
		super(number);
	}
	
	public String toString() {
		
		return super.toString();
	}
	
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) {
			
			return false;
		} else if (!(obj instanceof Abstract2HomePhone)) {
			
			return false;
		}
		
		return true;
	}
	
	
	@Override
	public void call(Abstract2Phone phone) {

		System.out.println("Calling " + phone.getNumber());

	}

}
