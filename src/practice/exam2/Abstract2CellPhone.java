package practice.exam2;

public class Abstract2CellPhone extends Abstract2Phone {

	private String carrier;
	
	public Abstract2CellPhone() {
		
		super();
		carrier = "AT&T";
	}
	
	public Abstract2CellPhone(String number, String carrier) {
		
		super(number);
		this.carrier = carrier;
	}
	
	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	
	public String toString() {
		
		return super.toString() + ", Carrier: " + carrier;
	}
	
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) {
			
			return false;
		} else if (!(obj instanceof Abstract2CellPhone)) {
			
			return false;
		}
		
		Abstract2CellPhone cell = (Abstract2CellPhone)obj;
		
		if (!carrier.equals(cell.getCarrier())) {
			
			return false;
		}
		
		return true;
	}

	@Override
	public void call(Abstract2Phone phone) {
		System.out.println("Calling " + phone.getNumber());

	}

}
