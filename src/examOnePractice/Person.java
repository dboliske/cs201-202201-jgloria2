package examOnePractice;

public class Person {

	
	private String name;
	
	private int age;
	
	public Person() {
		
		name = "Jonathan";
		
		age = 21;
	}
	
	public Person(String name, int age) {
		
		this.name = name;
		
		this.age = age;
	}
	
	public void setName(String name) {
		
		this.name = name;
	}
	
	public void setAge(int age) {
		
		this.age = age;
	}
	
	public String getName() {
		
		return name;  //We don't need to use this.name here because there is no name variable in the parameter, correct? YES
	}
	
	public int getAge() {
		
		return age;
	}
	
	public boolean equals(Person p) {
		
		if (!this.name.equalsIgnoreCase(p.getName())) {  //you can use name rather than this.name
			
			return false;
		}
		
		else if (this.age != p.getAge()) {  //You can use age rather than this.age
			
			return false;
		}
		
		else {
			
			return true;
		}
	}
	
	public String toString() {
		
		return "Name = " + name + " , Age = " + age;
	}
	
	public void haveBirthday() {
		
		age++;  //Could have also done age = age + 1;
	}
	
	
}
