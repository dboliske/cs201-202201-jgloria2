package examOnePractice;

public class Date {

	private int day;
	
	private int month;
	
	private int year;
	
	
	public Date() {
		
		day = 1;
		
		month = 1;
		
		year = 1970;
	}
	
	public Date(int day, int month, int year) {
		
		//this.day = day;  //Do I need to also validate the user's data here or only in the setters? YES, use the setters here, that is the point of the setters having validation
		
		//this.month = month;
		
		setDay(day);  //This is the correct way
		
		setMonth(month);
		
		this.year = year;
	}
	
	public void setDay(int day) {
		
		if (1<= day && day <= 31) {
			
			this.day = day;
		}
		
		else {
			
			System.out.print("Please enter a valid day between 1 and 31. You entered: " + day);
		}
	}
	
	public void setMonth(int month) {
		
		if (1<= day && day <= 12) {
			
			this.month = month;
		}
		
		else {
			
			System.out.print("Please enter a valid month between 1 and 12. You entered: " + month);
			
			this.day = 1;  //Remember to also have a default case so that the variable always has a value even if the user's value is invalid
		}
	}
	
	public void setYear(int year) {
		
		this.year = year;
	}
	
	public int getDay() {
		
		return day;
	}
	
	public int getMonth() {
		
		return month;
	}
	
	public int getYear() {
		
		return year;
	}
	
	public boolean equals(Date d) {
		
		if (this.day != d.getDay()) {
			
			return false;
		}
		
		else if(this.month != d.getMonth()) {
			
			return false;
		}
		
		else if(this.year != d.getYear()) {
			
			return false;
		}
		
		else {
			
			return true;
		}
	}
	
	public String toString() {
		
		return day + "-" + month + "-" + year;
	}
	
	
	
	
	
	
}
