package examOnePractice;

import java.util.Scanner;

public class DataType2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a character: "); //Usually the user input from a scanner is a string, but if they only enter in one character is its datatype a char? NO
		
		//int value = Integer.parseInt(input.nextLine());  //Is there another way to convert to integer? This doesn't return the ASCII value of the character, but converts a number that is a string into an int datatype
		
		char user = input.nextLine().charAt(0);
		
		int value = (int)user;  //Do I need to add (int) typecast here or is that redundant? YES I DO NEED TO
		
		char newchar =  (char)((value * 3) - 65);
		
		System.out.println("The new character is: " + newchar );
		
		input.close();

	}

}
