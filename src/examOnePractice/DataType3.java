package examOnePractice;

import java.util.Scanner;

public class DataType3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter in an integer: ");
		
		int num1 = Integer.parseInt(input.nextLine());
		
		//int num1 = input.nextInt();  //This is another way to covert the scanner input (which is a string) to an int
		
		System.out.print("Enter in another integer: ");
		
		int num2 = Integer.parseInt(input.nextLine());
		
		//int num2 = input.nextInt();
		
		double answer = (double)num1 / (double)num2;    //This isn't returning the fraction, it is just converting the int division to a double. 
														//After adding the (double) typecast, it now works 
		
		System.out.println("The result of the division is: " + answer);
		
		input.close();
		
		
	}

}
