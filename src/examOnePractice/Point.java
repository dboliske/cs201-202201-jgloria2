package examOnePractice;

public class Point {

	private double x;
	
	private double y;
	
	public Point() {
		
		x = 0;
		y = 0;
	}
	
	
	public Point(double x, double y) {
		
		this.x = x;
		
		this.y = y;
	}
	
	
	public void setX(double x) {
		
		this.x = x;
	}
	
	public void setY(double y) {
		
		this.y = y;
	}
	
	public double getX() {
		
		return x;
	}
	
	public double getY() {
		
		return y;
	}
	
	public boolean equals(Point p) {
		
		//if (x != p.getX()) {  //For doubles we need to use the Math.abs method and if the difference is greater that 0.001 then the doubles aren't equal
			
		if (Math.abs(x - p.getX()) > 0.001) {  //correct way to check equality for doubles
			return false;
		}
		
		//else if (y != p.getY()) {  //Same applies here
		
		else if (Math.abs(y - p.getY()) > 0.001) {  //correct way to check equality for doubles
			
			return false;
		}
		
		else {
			
			return true;
		}
	}
	
	
	public String toString() {
		
		return "(" + x + "," + y + ")";
	}
	
	public double distance(Point p) {
		
		//return ((this.x - p.getX()) * (this.x - p.getX())) +  ((this.y - p.getY()) * (this.y - p.getY()));  //How do I format the return?
		
		return Math.sqrt(Math.pow(x - p.getX(), 2) + Math.pow(y - p.getY(), 2));
		
	}
	
}
