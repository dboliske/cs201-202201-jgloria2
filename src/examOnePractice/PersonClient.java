package examOnePractice;

public class PersonClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Person p1 = new Person();
		
		System.out.println(p1.toString());
		
		Person p2 = new Person("Jonathan", 20);
		
		System.out.println(p2.toString());
		
		System.out.println(p1.equals(p2));
		
		p1.haveBirthday();
		
		System.out.println(p1.toString());
	}

}
