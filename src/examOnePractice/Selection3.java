package examOnePractice;

import java.util.Scanner;

public class Selection3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter the letter 'a', 'b', 'c', or 'd':");
		
		String choice = input.nextLine(); //You can also convert it to a char and also to lowercase to avoid exceptions
		
		switch(choice) {
		
		case "a" :
			
			System.out.println("apple");
			
			break;
			
			
		case "b" :
			
			System.out.println("bat");
			
			break;
			
			
			
		case "c":
			
			System.out.println("cat");
			
			break;
			
			
			
		case "d":
			
			System.out.println("disk");
			
			
			break;
			
			
		default:  
			
			System.out.println("You failed!");
		
		
		}
		
		
		
		
		
		
		
		
		
		
		
		input.close();
		
	}

}
