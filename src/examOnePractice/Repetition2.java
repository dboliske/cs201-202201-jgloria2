package examOnePractice;

import java.util.Scanner;

public class Repetition2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Scanner input = new Scanner(System.in);
		
		boolean running = true;
		
		while(running) {
			
		
		
		System.out.print("Enter either addition, subtraction, multiplication, or division: ");
		
		String choice = input.nextLine().toLowerCase();
		
		if (choice.equals("addition")) {
			
			System.out.print("Enter a number: ");
			
			double num1 = Double.parseDouble(input.nextLine());
			
			System.out.print("Enter a number: ");
			
			double num2 = Double.parseDouble(input.nextLine());
			
			System.out.println("The result is: " + (num1 + num2));
			
			running = false;
		}
		
		else if (choice.equals("subtraction")) {
			
			System.out.print("Enter a number: ");
			
			double num1 = Double.parseDouble(input.nextLine());
			
			System.out.print("Enter a number: ");
			
			double num2 = Double.parseDouble(input.nextLine());
			
			System.out.println("The result is: " + (num1 - num2));
			
			running = false;
			
		}
		
		
		else if (choice.equals("multiplication")) {
			
			System.out.print("Enter a number: ");
			
			double num1 = Double.parseDouble(input.nextLine());
			
			System.out.print("Enter a number: ");
			
			double num2 = Double.parseDouble(input.nextLine());
			
			System.out.println("The result is: " + (num1 * num2));
			
			running = false;
			
		}
		
		else if (choice.equals("division")) {
			
			System.out.print("Enter a number: ");
			
			double num1 = Double.parseDouble(input.nextLine());
			
			System.out.print("Enter a number: ");
			
			double num2 = Double.parseDouble(input.nextLine());
			
			System.out.println("The result is: " + (num1 / num2));
			
			running = false;
			
		}
		
		
		else {
			
			System.out.println("Invalid choice!");
		}
		
		
		}
		
		
		
		
		
		
		
		input.close();
		
		

	}

}
