package examOnePractice;

import java.util.Scanner;

public class Selection1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter an integer: ");
		
		int num = Integer.parseInt(input.nextLine());
		
		if ((num % 2) == 0) {  //Notice we use == rather than = since if-else takes a boolean condition
			
			System.out.println("The number is even and the result is: " + (num/2));
		}
		
		else { 
			
			System.out.println("The number is odd and the result is: " + ((3 * num) + 1));  //You don't need the parenthesis around 3 * num because order of 
																							//operations does multiplication before adding 1 so (3 * num + 1) works
		}																					
		
		input.close();
		
	}

}
