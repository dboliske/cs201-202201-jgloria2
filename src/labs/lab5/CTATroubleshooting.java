package labs.lab5;

import java.io.File;
import java.util.Scanner;

public class CTATroubleshooting {  //PLEASE IGNORE: This class was made solely for troubleshooting

	public static void main(String[] args) {  
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

	//	System.out.print("Enter a filename: ");
		
	//	String filename = input.nextLine();
		
		String filename = "src/labs/lab5/CTAStops.csv";
		
		for (int i = 0; i < readFile(filename).length; i++) {  

			System.out.println(readFile(filename)[i].toString());  //Testing to ensure the file was actually passed through
			
		
		}
		
		//menu(readFile(filename));
		
		
		input.close();
	}

	public static CTAStation[] readFile(String filename) {  //Why do we use static for all of the methods in the client class?
		
		
		CTAStation[] stations = new CTAStation[10];
		
		int count = 0;
		
		try {
			File f = new File(filename);
			
			Scanner input = new Scanner(f);
			
			while(input.hasNextLine()) {
				
				String line = input.nextLine();
				
				String[] values = line.split(",");
				
				CTAStation alt = null;
				
				if (stations.length == count) {
					
					stations = resize(stations, stations.length * 2);
					
				}
				
				//CTAStation alt = new CTAStation(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]), values[3], Boolean.parseBoolean(values[4]), Boolean.parseBoolean(values[5]));
				
				//Could I use the above line instead of splitting into two lines (setting alt equal to null and then assigning the values on a different line)
				
				alt = new CTAStation(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]), values[3], Boolean.parseBoolean(values[4]), Boolean.parseBoolean(values[5]));
				
				stations[count] = alt;  //Set this equal to an instance of stations rather than to the input. Do this be creating a temp instance of stations and splitting and parsing the input into that temp stations.
				
				count++;
				
			}
			
						
			
			
			input.close();
		
	
		} catch (Exception e){
			 
			 e.getMessage();
			 
		 }
		
		stations = resize(stations, count);
		
		return stations;
		
	}
	
	public static CTAStation[] resize(CTAStation[] stations, int size) {  //Additional method for resizing arrays
		
		CTAStation[] temp = new CTAStation[size];
		
		int limit = stations.length > size ? size : stations.length;  //This is to avoid going past the index of the array. If we are trimming, we use size as the limit. If we are resizing, we use stations.length as the limit
		
		for (int i = 0; i < limit; i++) {
			
			temp[i] = stations[i];
		}
		
		/*stations = temp;   //Why don't I have to use this line? Usually for resizing you do
		
		temp = null;*/
		
		return temp;  
	}
		
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
