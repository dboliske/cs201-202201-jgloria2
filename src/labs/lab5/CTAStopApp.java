package labs.lab5;

import java.io.File;
import java.util.Scanner;

//import labs.lab4.GeoLocation;  //Do I need to import the superclass since its in a different package?

public class CTAStopApp {

	public static void main(String[] args) {  //main method
		
		Scanner input = new Scanner(System.in);

		System.out.print("Enter a filename: ");
		
		String filename = input.nextLine();
		
		//String filename = "src/labs/lab5/CTAStops.csv";
		
		//readFile(filename);
		
		menu(readFile(filename));  //Is this wrong? I am trying to pass the CTAStation[] returned from readFile to the menu. NO THIS WORKS
		/*
		 * //
		 * 
		 * CTAStation[] stations = new CTAStation [readFile(filename).length];     //Please ignore
		 * 
		 * for (int i = 0; i < stations.length; i++) {
		 * 
		 * stations[i] = readFile(filename)[i];
		 * 
		 * System.out.println(stations[i].toString());
		 * 
		 * }
		 * 
		 * menu(stations);
		 */
//		
	
		input.close();
	}

	public static CTAStation[] readFile(String filename) {  //readFile method      //Why do we use static for all of the methods in the client class? -----> Because they must be static to be used in the main method
		
		
		CTAStation[] stations = new CTAStation[10];
		
		int count = 0;
		
		try {
			File f = new File(filename);
			
			Scanner input = new Scanner(f);
			
			while(input.hasNextLine()) {
				
				String line = input.nextLine();
				
	//			System.out.println(line);
				
				String[] values = line.split(",");
				
				if (!values[0].equals("Name")) {  //Could have checked to make sure values[1] is a boolean
				
				CTAStation alt = null;
				
				alt = new CTAStation(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]), values[3], Boolean.parseBoolean(values[4]), Boolean.parseBoolean(values[5]));
				
				
				
				//CTAStation alt = new CTAStation(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]), values[3], Boolean.parseBoolean(values[4]), Boolean.parseBoolean(values[5]));
				
				//Could I use the above line instead of splitting into two lines (setting alt equal to null and then assigning the values on a different line)
				
								
				if (stations.length == count) {
					
					stations = resize(stations, stations.length * 2);
					
				}

				stations[count] = alt;  //Set this equal to an instance of stations rather than to the input. Do this be creating a temp instance of stations and splitting and parsing the input into that temp stations.
				
				count++;
				}
			}
			
//			System.out.println("Done");			
			
			
			input.close();
		
	
		} catch (Exception e){
			 
			 System.out.println(e.getMessage());
			 
		 }
		
		stations = resize(stations, count);
		/*
		 * for (int i = 0; i < stations.length; i++) {
		 * 
		 * System.out.println(stations[i].toString()); }
		 */
		return stations;
		

	}
	
	public static CTAStation[] resize(CTAStation[] stations, int size) {  //Additional method for resizing arrays
		
		CTAStation[] temp = new CTAStation[size];
		
		int limit = stations.length > size ? size : stations.length;  //This is to avoid going past the index of the array. If we are trimming, we use size as the limit. If we are resizing, we use stations.length as the limit
		
		for (int i = 0; i < limit; i++) {
			
			temp[i] = stations[i];
		}
		
		/*stations = temp;   //Why don't I have to use this line? Usually for resizing you do
		
		temp = null;*/
		
		return temp;  
	}
	
	
	public static void menu(CTAStation[] stations) {  //method for menu
		
	//public static CTAStation[] menu(CTAStation[] stations) {
		
		Scanner input = new Scanner(System.in);
		
		boolean running = true;
				
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Station with/without Wheelchair Access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			
			String choice = input.nextLine();
			
			switch(choice) {
			
			case "1" :
				
				displayStationNames(stations);
				
				break;
				
			case "2" :
				
				displayByWheelchair(input, stations);
				
				break;
				
			case "3" :
				
				displayNearest(input, stations);
				
				break;
				
			case "4" :
				
				running = false;
				
				System.out.println("Goodbye!");
				
				break;
				
			default :
				
				System.out.println("'" + choice + "' is not a valid choice");
			
			
			}
			
			
		} while (running);
		
		
	}
	
	public static void displayStationNames(CTAStation[] stations) {  //method which displays station names
		
		for (int i = 0; i < stations.length; i++) {
			
			System.out.println(stations[i].getName());
			
			}
		
		
	}
	
	public static void displayByWheelchair(Scanner input, CTAStation[] stations) {  //method which prompts for wheelchair accessibility and displays the corresponding stations
		
		boolean running = true;
		
		boolean wheelchair = false;
		
		do { 
			
			System.out.print("Do you need wheelchair access? (y/n) :  ");
			
			String choice = input.nextLine().toLowerCase();
			
			switch(choice) {
			
			case "y" :
			case "yes" :
				
				wheelchair = true;
				
				running = false;
				
				break;
				
			case "n" :
			case "no" :
				
				wheelchair = false;
				
				running = false; 
						
				break;
				
			default : 
				
				System.out.println("'" + choice + "' is not a valid choice.");
			
			}
			
			
		} while (running);
		
		int stationsCount = 0;
		
		for (int i = 0; i < stations.length; i++) {
			
			if (stations[i].hasWheelchair() == wheelchair) {
			System.out.println(stations[i].getName());
			
			stationsCount++;
			
			}
		}
		
		if (stationsCount == 0) {
			
			System.out.println("Sorry there are no available stations.");
		}
		
	}
	
	public static void displayNearest(Scanner input, CTAStation[] stations) {  //method which displays the nearest station to the user's inputed lat and long
		
		System.out.print("Enter a latitude: ");
		
		double lat = Double.parseDouble(input.nextLine());
		
		System.out.print("Enter a longitude: ");
		
		double lng = Double.parseDouble(input.nextLine());
		
		
		/*
		 * double nearest = stations[0].calcDistance(stations[1].getLat(),
		 * stations[1].getLng()); //TODO: Make sure this is the correct format
		 * 
		 * for (int i = 0; i < stations.length; i++) {
		 * 
		 * double temp = stations[i].calcDistance(stations[i+1].getLat(),
		 * stations[i+1].getLng());
		 * 
		 * if (nearest > temp) {
		 * 
		 * nearest = temp; }
		 * 
		 * }
		 */
		
		double nearest = stations[1].calcDistance(lat, lng);  //TODO: Make sure this is the correct format
		
		int nearestIndex = 0;
		
		for (int i = 1; i < stations.length; i++) {
			
			double temp = stations[i].calcDistance(lat, lng);
			
			if (nearest > temp) {
				
				nearest = temp;
				
				nearestIndex = i;
			}					
			
		}
		
		
		System.out.println(stations[nearestIndex].getName());
		
		
		
	}
	
	
	
	
	
}
