package labs.lab5;

import labs.lab4.GeoLocation;  //Do I need to import since GeoLocation is in a different package?

public class CTAStation extends GeoLocation{  

	
	private String name;    //Instance variables
	
	private String location;
	
	private boolean wheelchair;
	
	private boolean open;
	
	public CTAStation() {  //Default constructor
		
		super();
		
		name = "35th";
		
		location = "elevated"; 
		
		wheelchair = false;
		
		open = false;
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {  //non-default constructor
		
		
		super(lat, lng);
		
		this.name = name;
		
		this.location = location;
		
		this.wheelchair = wheelchair;
		
		this.open = open;
		
	}
	
	
	public String getName() {  //accessor method for name
		
		return name;
	}
	
	public String getLocation() {  //accessor method for locations
		
		return location;
	}
	
	
	public boolean hasWheelchair() {  //accessor method for wheelchair
		
		return wheelchair;
	}
	
	public boolean isOpen() {  //accessor method for open
		
		return open;
	}
	
	public void setName(String name) {  //mutator method for name
		
		this.name = name;
	}
	
	public void setLocation(String location) {  //mutator method for location
		
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {  //mutator method for wheelchair
		
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) { //mutator method for open
		
		this.open = open;
	}
	
	@Override
	public String toString() {  //toString method
		
		return name + ", " + super.getLat() + ", " + super.getLng() + ", " + location + ", " + wheelchair + ", " + open;  //TODO:Change this format, more descriptive
	}
	
	
	
	  @Override 
	  public boolean equals(Object obj) { 	 //equals method    //Is there supposed to be a parameter? YES an object parameter
	  
	  if (!super.equals(obj)) {
		  
		  return false;
	  } else if (!(obj instanceof CTAStation)){
		  
		  return false;
	  } 
		  
		  CTAStation station = (CTAStation)obj;
		  
	  
	  
	  
	  
	  if (!this.name.equals(station.getName())) {
		  
		  return false;
	  }else if (!this.location.equals(station.getLocation())){
		  
		  return false;
	  }else if (this.wheelchair != station.hasWheelchair()){
		  
		  return false;
	  }else if (this.open != station.isOpen()){
		  
		  return false;
	  }else {
		  
		  return true;
	  }
	  
	  
	  }
	 
	
	
	
	
	
	
}
