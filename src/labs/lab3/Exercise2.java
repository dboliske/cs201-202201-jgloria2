package labs.lab3;

import java.io.FileWriter;
import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);  //Variable for reading user input

		
		double[] numbers = new double[10];  //initializing array
		int count = 0;  //initializing a variable to count the amount of numbers the user inputs
		boolean running = true;  //flag variable


		while (running) {  

			System.out.print("Enter a number. When finished enter done: ");  //Prompting user for input
			String value = input.nextLine();  //reading input

			if (value.equalsIgnoreCase("done")) {  

				running = false;  //stopping the loop if the user inputs "done"

			}

			else {

				if (count == numbers.length) { // Resizing the array

					double[] bigger = new double[2 * numbers.length];
					for (int i = 0; i < numbers.length; i++) {
						bigger[i] = numbers[i];

					}
					numbers = bigger;
					bigger = null;

				}

				numbers[count] = Double.parseDouble(value); //storing the value in the array
				count++;

			}

			//count++;  //Why did my array have an extra zero element as its last element when I had the count here rather than in the else statement?

		}

		

		
			double[] smaller = new double[count];  //Trimming the array

			for (int i = 0; i < count; i++) {
				smaller[i] = numbers[i];
			}
			numbers = smaller;
			smaller = null;	


		  
		  System.out.print("Enter a filename: "); //Prompting user for a filename
		   String fileName = input.nextLine();  //Storing filename in a string
		   
		   
		   try {   //Using try/catch blocks to avoid IOExceptions
			   
			   FileWriter f = new FileWriter("src/labs/lab3/" + fileName);  //Constructing a new file
			   
			   for (int i = 0; i<numbers.length; i++) { 
				   
				   f.write(numbers[i] + "\n");  //Writing all of the values from the array to the file
				   				   
			   }
			   
			   f.flush();  //Flushing the filewriter
			   f.close();  //closing the filewriter
			   
			   System.out.println("Your values have been saved to the " + fileName + " file.");  //Notifying the user that the file was successfully created
			   
		   } catch (Exception e) {
			   
			   System.out.println(e.getMessage());  //Prints out error message if an exception occurs
			   
		   }
		  
		 
		input.close(); //closing the scanner
		
		

	}

}
