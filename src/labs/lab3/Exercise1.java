package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) throws IOException {
		
		File f = new File("src/labs/lab3/grades.csv"); //Creating a file from grades.csv
		
		Scanner input = new Scanner(f);  //Creating a scanner to read the file
		
		int[] grades = new int[14];  //Creating an int array of length 14 since there are 14 grades in the file
		int count = 0;
		
		while (input.hasNextLine()) {  //This loop is to read in the file
			
			String[] data = input.nextLine().split(",");  //Created a string array and set it equal to the data on the next line of the file. It is split into two elements at the comma 
			grades[count] = Integer.parseInt(data[1]);  //Setting the grades array to the 2nd element of the data array since that is where the grades are located
			count++;
			
		}
		double Total = 0;
		
		for (int i=0; i<grades.length; i++) {  //Using a for loop to run through each array element of the grades array
			
			//System.out.print(grades[i] + ", ");
			Total = Total + grades[i];  //Adding each grade to the total
		}
		
		System.out.println("The average grade for the class is " + (Total/count) + " percent"); //Printing and calculating the average class grade
		
		input.close();
		
		
		

	}

}
