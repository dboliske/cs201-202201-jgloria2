package labs.lab3;

public class Exercise3 {

	public static void main(String[] args) {
		
		int[] values = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};  //Declaring an array with all of the provided values
		
		int Min = values[0];  //Assigning the first element of the array as the minimum value
		
		for (int i=0; i<values.length; i++) {
			
			if (Min > values[i]) {  //Comparing each element of the array to the minimum to see if any value is less that the minimum
				Min = values[i];  //Setting a value which is less than the previous minimum as the new minimum
			}
		}
		
		System.out.println("The minimum value is: " + Min);  //Printing the minimum
		
	}

}
