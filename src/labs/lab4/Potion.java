package labs.lab4;

public class Potion {

	private String name;  //Instance variable for name
	
	private double strength;  //Instance variable for strength
	
	public Potion() {  //Default constructor
		
		name = "Health";
		
		strength = 5.0;
	}
	
	public Potion(String name, double strength) {  //Non-default constructor
		
		
		this.name = name;
		
		this.strength = strength;
	}
	
	public String getName() {  //Accessor method for name
		
		return name;
	}
	
	
	public double getStrength() {  //Accessor method for strength
		
		return strength;
	}
	
	public void setName(String name) {  //Mutator method for name
		
		this.name = name;
	}
	
	public void setStrength(double strength) {  //Mutator method for strength
		
		this.strength = strength;
	}
	
	public String toString() {  //toString method to return the values as a string
		
		return "Name = " + name + ", Strength = " + strength;
	}
	
	//Below is an alternate method for validStrength. Please ignore
	/*
	 * public boolean validStrength(double strength) { //Method that returns true if
	 * the strength is between 0 and 10
	 * 
	 * if (0 <= strength && strength <= 10) {
	 * 
	 * return true; }
	 * 
	 * else {
	 * 
	 * return false; } }
	 */
	
	
public boolean validStrength() {  //Method that returns true if the strength is between 0 and 10
		
		if (0 <= this.strength && this.strength <= 10) {
			
			return true;
		}
		
		else {
			
			return false;
		}
	}


	
	public boolean equals(Potion p) {  //Equals method which compares this instance to another Potion
		
		//if (name != p.getName()) {  //Since this is a String don't use "!=" but instead use ! equalsIgnoreCase 
		
		if (!name.equalsIgnoreCase(p.getName())) {
			
			return false;
		}
		else if (strength != p.getStrength()) {
			
			return false;			
		}
		else {
			
			return true;
		}
	}
	
}
