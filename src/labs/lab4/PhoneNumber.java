package labs.lab4;

public class PhoneNumber {

	private String countryCode;  //Creating the countryCode instance variable
	
	private String areaCode;  //Creating the areaCode instance variable
	
	private String number;  //Creating the number instance variable
	
	
	public PhoneNumber() {  //Default constructor
		
		countryCode = "0";
		areaCode = "000";
		number = "0000000";
	}
	
	public PhoneNumber(String cCode, String aCode, String number) {  //Non-default constructor
		
		this.countryCode = cCode;
		this.areaCode = aCode;
		this.number = number;
	}
	
	public String getCountryCode() {  //Accessor method for countryCode
		
		return countryCode;
	}
	
	
	public String getAreaCode() {  //Accessor method for areaCode
		
		return areaCode;
	}
	
	public String getNumber() {  //Accessor method for number
		
		return number;
	}
	
	
	public void setCountryCode(String cCode) {  //Mutator method for countryCode
		
		this.countryCode = cCode; 
	}

	public void setAreaCode(String aCode) {  //Mutator method for areaCode
	
		this.areaCode = aCode;
	}
	
	public void setNumber(String number) {  //Mutator method for number
		
		this.number = number;
	}
	
	public String toString() {  //toString method which returns the entire phone number as a String
		
		return countryCode + "-(" + areaCode + ")-" + number; 
	}
	
	//Below is another way to code validAreaCode, please ignore
	/*
	 * public boolean validAreaCode(String aCode) { 
	 * 
	 * this.areaCode = aCode; //Why don't I need this line?
	 * 
	 * if (aCode.length() == 3) {
	 * 
	 * return true; } else {
	 * 
	 * return false; } }
	 */
	
	
	  public boolean validAreaCode() { //Method which returns true if the area code is 3 characters
	  
	  
		  if (this.areaCode.length() == 3) {
	  
		  return true; 
		  
		  }
	  
		  else {
	  
			  return false;
		  }
	  }
	 
	  
	  //Below is another way to code validNumber, please ignore
	
/*
 * public boolean validNumber(String number) { //Method which returns true if
 * the number is 7 characters
 * 
 * //this.number = number; //Why don't I need this line?
 * 
 * if(number.length() == 7) {
 * 
 * return true; } else { return false; } }
 */
	  
	  
	 public boolean validNumber() {  //Method which returns true if the number is 7 characters
		
		
		if(this.number.length() == 7) {
			
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean equals(PhoneNumber pn) {  //Method which compares two instances of the class
		
		if (!countryCode.equals(pn.getCountryCode())) {
			
			return false;
		}
	
		else if (!areaCode.equals(pn.getAreaCode())) {
			
			return false;
		}
		else if (!number.equals(pn.getNumber())) {
			
			return false;
		}
		else {
			return true;
		}
		
	}
	
	
	
	
}
	
