package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		
		Potion p1 = new Potion();  //First instance of Potion using the default constructor
		
		System.out.println(p1.toString());
		
		
		Potion p2 = new Potion("Health",5);  //Second instance of Potion using the non-default constructor
		
		System.out.println(p2.toString());
		
		
		//Additional testing
		
		
		System.out.println(p1.equals(p2));  //Testing equals method
		
		System.out.println(p2.validStrength());  //Testing validStrength
		
		//System.out.println(p1.validStrength(p2.getStrength()));  //Testing alternate validStrength (Please Ignore)
		
		
		
		
		
		

	}

}
