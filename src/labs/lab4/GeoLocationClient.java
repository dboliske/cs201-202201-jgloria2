package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		

		GeoLocation gl1 = new GeoLocation();  //First instance of GeoLocation using the default constructor
		
		gl1.setLat(89);
		gl1.setLng(20);
		
		
		System.out.println("The lattitude is " + gl1.getLat() + " and the longitude is " + gl1.getLng());  //Displaying the values of the instance variables using accessor methods
		
		GeoLocation gl2 = new GeoLocation(5, 179);  //Second instance of GeoLocation using the non-default constructor
		
		
		System.out.println("The lattitude is " + gl2.getLat() + " and the longitude is " + gl2.getLng());  //Displaying the values of the instance variables using accessor methods
	
	
		
		
		
		
		
		
		//Additional Tests Below
	
	// I was supposed to use these methods to validate the mutators and non-default constructor//	System.out.println(gl1.validLat());  // Testing validLat    //What happens if I put gl1.lat90(gl2) or vice versa? Essentially what happens if I mismatch the GeoLocations and does the order matter
		
	//	                                                                                            System.out.println(gl2.validLng());  //Testing validLng
		
		System.out.println(gl1.toString());  //Testing toString
	
	}

}
