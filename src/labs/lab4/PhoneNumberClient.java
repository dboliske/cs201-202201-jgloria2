package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		
		
		PhoneNumber pn1 = new PhoneNumber();  //First instance of PhoneNumber using the default constructor
		
		System.out.println(pn1.toString());  //Displaying the value using toString method
		
		PhoneNumber pn2 = new PhoneNumber("1", "847", "9237236");  //Second instance of PhoneNumberusing the non-default constructor
		
		//PhoneNumber pn2 = new PhoneNumber("0", "000", "0000000");  //Used this to test equals method
		
		System.out.println(pn2.toString());  //Displaying the value using toString method

		
		
		//Additional Tests Below
		
		
		//System.out.println(pn2.validAreaCode(pn2.getAreaCode()));  //Testing alternate validAreaCode (please ignore)          //What happens if I put pn2.validAreaCode(pn1.getAreaCode()) or vice versa? Essentially what happens if I mismatch the GeoLocations and does the order matter
		
		System.out.println(pn2.validAreaCode());  //Testing validAreaCode
		
		//System.out.println(pn2.validNumber(pn2.getNumber()));  //Testing alternate validNumber (please ignore)
		
		System.out.println(pn2.validNumber());  //Testing validNumber
		
		System.out.println(pn1.equals(pn2));  //Testing equals
		
	}

}
