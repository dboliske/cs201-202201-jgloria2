package labs.lab4;

public class GeoLocation {

		
	private double lat;  //Instance variables
	private double lng;
	
	
	public GeoLocation() {  //Default constructor
		
		lat = 0.0;
		lng = 0.0;
	
	}
	
	public GeoLocation(double lat, double lng) {  //Non-default constructor
		
		setLat(lat);
		setLng(lng);
		
	}
	
	public double getLat() {  //Accessor method for lat
		
		return lat;
	}
	
	public double getLng() {  //Accessor method for lng
		
		return lng;
	}
	
	public void setLat(double lat) {  //Mutator method for lat
		
		if (validLat(lat)) {
			
		
		this.lat = lat;
	
		} else {
			this.lat = 0.0;
		}
		
		
	}
	public void setLng(double lng) {  //Mutator method for lng
		
		if (validLng(lng)) {
		this.lng = lng;		
		} else {
			this.lat = 0.0;
		}
	} 

	public String toString() {  //toString method to return the location in the format "(lat, lng)"
		
		return "(" + lat + ", " + lng + ")";
	}

	public boolean validLat(double lat) {  //Method which returns true if the lat is between -90 and 90
		
		//if (-90 <= gl.getLat() && gl.getLat() <= 90) {  //Does "between -90 and 90" include -90 and 90?
		//	if (-90 <= this.lat && this.lat <= 90) {
		
		if (-90 <= lat && lat <= 90) {
			
			return true;		
		} 
		
		else { 
			
			return false;
		}
	}
	
	
	public boolean validLng(double lng) {  //Method which returns true if the lng is between -180 and 180
		
		//if (-180 <= this.lng && this.lng <= 180) {  //Does "between -180 and 180" include -180 and 180?
		
		if (-180 <= lng && lng <= 180) {	
			
			return true;
		}
		
		else {
			
			return false;
			
		}
	}
	
	public boolean equals(GeoLocation gl){  //Method which compares this instance to another Geolocation
		
		if (this.lat != gl.getLat()) {
			
			return false;
		}
		else if (this.lng != gl.getLng()) {
			
			return false;
		}
		else {
			
			return true;
		}
	}
	
	
	public double calcDistance(GeoLocation gl) {  //calc distance method which takes a GeoLocation as parameter
		
		double distance = 0.0;
		
		distance = Math.sqrt(Math.pow(this.lat - gl.getLat(), 2) + Math.pow(this.lng - gl.getLng(), 2));
		
		
		return distance;
	}
	
	public double calcDistance(double lat, double lng) {  //calc distance method which takes a lat and long as parameter
		
		double distance = 0.0;
		
		distance = Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
		
		
		return distance;
	}
	
	
	
	
	

}
	

