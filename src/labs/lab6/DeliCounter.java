package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounter {

	public static void main(String[] args) {  //Main method
		
		ArrayList<String> deliQueue = new ArrayList<String>();  //ArrayList for the deli queue
		
		Scanner input = new Scanner(System.in);
		boolean running = true;
		
		do {
			
			System.out.println("1. Add Customer");  //menu
			System.out.println("2. Help Customer");
			System.out.println("3. Exit");
			System.out.print("Choice: ");
			
			String choice = input.nextLine();  // How to ignore case? (if the string wasn't an int):     USE .toLowerCase()
			
			switch(choice) {
			
			case "1" :   //adding customer
				
				System.out.println(addCustomer(deliQueue, input));  //Is there a way to display the string which the method returns without having to print it?
				
				break;
				
			case "2" :  //helping customer
				
				System.out.println(helpCustomer(deliQueue));  //Is there a way to display the string which the method returns without having to print it?
				
				break;
				
			case "3" :  //exiting
				
				running = false;
				System.out.println("Exiting...");
				
				break;
				
			default :
				
				System.out.println("'" + choice + "' isn't a valid input");
			
			
			}
			
			
		} while(running);
		
		
		input.close();

	}
	
	
	public static String addCustomer(ArrayList<String> deliQueue, Scanner input) {  //Method for adding customers
		
		System.out.print("Enter the customer's name: ");
		
		String name = input.nextLine();
		
		deliQueue.add(name);
		
		int position = deliQueue.size() - 1;  //Position is 1 less than the total size
		
		return "Position: " + position;
	}
	
	
	public static String helpCustomer(ArrayList<String> deliQueue) {  //Method for helping customers
		
		if (deliQueue.size() == 0) {
			
			return "No available customers";
		}
		
		return "Helping: " + deliQueue.remove(0);
	}
	
	
	/*
	 * public static void exit(boolean running) { //Ignore. Method not needed
	 * 
	 * running = false;
	 * 
	 * System.out.println("Exiting...");
	 * 
	 * }
	 */
	 
	

}
