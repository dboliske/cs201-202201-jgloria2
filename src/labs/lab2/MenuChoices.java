package labs.lab2;

import java.util.Scanner;

public class MenuChoices {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);   //Variable for reading user input
	
		boolean running = true;  //Initializing flag
		
		while (running) {    //Flag controlled loop
			
			System.out.println("1. Say Hello");  //Option 1 of menu
			System.out.println("2. Addition");  //Option 2 of menu
			System.out.println("3. Multiplication");  //Option 3 of menu
			System.out.println("4. Exit");  //Option 4 of menu
			System.out.print("Please choose an option by entering the corresponding number: ");  //Prompting user for choice of option
			String Choice = input.nextLine();  //Reading in the user's choice
			
			
			switch(Choice) {  //switch statement using the user's choice
			
			case "1":  //This case corresponds to the user selecting 1
			
				System.out.println("Hello!");  //Prints Hello 
				
				break;
				
			case "2":  //This case corresponds to the user selecting 2
				
				System.out.print("Enter a number: ");  //Prompt user for a number
				
				double num1 = Double.parseDouble(input.nextLine());  //Read in user's number
				
				System.out.print("Enter a second number: ");  //Prompt user for a second number 
				
				double num2 = Double.parseDouble(input.nextLine());  //Read in user's second number
				
				System.out.println("The sum of the two numbers is " + (num1 + num2));  //Prints the sum of the 2 numbers
				
				break;
							
			case "3":  //This case corresponds to the user selecting 3
				
				System.out.print("Enter a number: ");  //Prompt user for a number
				
				double num3 = Double.parseDouble(input.nextLine());  //Read in user's number
				
				System.out.print("Enter a second number: ");  //Prompt user for a second number
				
				double num4 = Double.parseDouble(input.nextLine());  //Read in user's second number
				
				System.out.println("The product of the two numbers is " + (num3 * num4));  //Prints the product of the 2 numbers
				
				break;
			
			case "4":  //This case corresponds to the user selecting 4
				
				System.out.println("Goodbye!");  //Prints Goodbye
				
				running = false;  //Changes the flag variable so that the flag controlled loop stops
				
				break;
			
			default:  //This case corresponds to an invalid input
				
				System.out.println("Invalid input! Please enter either 1, 2, 3, or 4");  //Prints a statement which tells the user they need to input a valid input
				
				break;
				
				
				
			}
	
		
		}
		
		
		
		input.close();  //Closing the scanner

	}

}
