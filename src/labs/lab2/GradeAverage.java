package labs.lab2;

import java.util.Scanner;

public class GradeAverage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);   //Variable for reading user input
		
		System.out.print("Enter an exam grade: ");  //Prompting user for first exam grade
		
		String grades = input.nextLine();  //Read in first exam grade
		
		double Sum = Double.parseDouble(grades) + 1;  //Setting the variable Sum equal to grades, which is converted to a double. I am adding 1 to the value since the user must subtract 1 to end the program, which would result in the sum being 1 less than the actual sum
		
		int Count = 0;  //Initializing the loop count to zero. This variable is equal to the amount of exam grades entered
		
		while (!grades.equalsIgnoreCase("-1")) {  //This while loop will run as long as the exam grade entered isn't -1
		
			System.out.print("Enter another exam grade. When finished enter -1: ");  //Prompting for another exam grade
			
			grades = input.nextLine();  //Setting the variable grades to the new exam grade
				
			Sum += Double.parseDouble(grades);  //Adding the new exam grade to the total sum
			
			Count++;  //Increasing the count of exam grades by 1
				
		}
		
		input.close();  //Closing the scanner
		
		
		System.out.println("The average of the exam grades is " + (Sum/Count) + " percent");  //Printing the average of the exam grades
	

	}

}
