package labs.lab2;

import java.util.Scanner;

public class NumberSquare {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Scanner input = new Scanner(System.in);   //Variable for reading user input
		
		System.out.print("Enter a number: ");  //Prompting user for a number
		
		int number = Integer.parseInt(input.nextLine());  //Reading in the user's number and converting it to a integer
		
		input.close();  //Closing the scanner
		
		int loopCount = 1;  //Setting the inner loop count to 1
		
		int outerLoopCount = 1;  //Setting the outer loop count to 1
		
		while (outerLoopCount <= number) {  //Outer while loop which allows for multiple lines of asterisks
		
			while (loopCount <= number) {  //Inner while loop which allows for multiple asterisks on a single line
		
				System.out.print("* ");  //Printing an asterisk 
		
				loopCount++;  //Increasing the inner loop's count by 1
		
			}
		
		System.out.println(" ");  //Using println to go to the next line of asterisks (Is there a better way to go to the next line of the output instead of printing a space?)
		
		outerLoopCount++;  //Increasing the outer loop's count by 1
		
		loopCount = 1;  //Resetting the inner loop's count to 1 so that it can be used in the inner while loop 
		
		}
		
		

	}

}
