package labs.lab1;

import java.util.Scanner;

public class CharInput {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in); //Variable for reading user input
		
		System.out.print("Enter your name:");  //Prompt user for their name
		
		String name = input.nextLine();  //Read in the user's name
		
		input.close();  //Closing the scanner
		
		System.out.println(name.charAt(0));  //Printing the first initial of the user's name
		
		
		
		
	}

}
