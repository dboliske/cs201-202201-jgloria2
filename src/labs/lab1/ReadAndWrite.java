package labs.lab1;

import java.util.Scanner;

public class ReadAndWrite {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in); //Variable for reading user input
		
		System.out.print("Enter your name:");  //Prompt user for their name
		
		String name = input.nextLine();  //Read in the user's name and save it as the variable "name"
		
		input.close();  //Closing the scanner
		
		System.out.println(name); //Printing out the user's name
		
	}

}
