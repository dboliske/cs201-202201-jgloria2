package labs.lab1;

public class ArithmeticCalc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		int myAge = 20; //Defining variable for my age
				
		int fatherAge = 51;  //Defining variable for my father's age
		
		int year = 2001;  //Defining variable for my birth year
		
		int height = 76;  //Defining variable for my height in inches
		
		System.out.println("The difference between my age and my father's age is " + (fatherAge - myAge) + " years"); //Subtracting my age from my father's age
		
		System.out.println("My birth year multiplied by two is " + (year * 2));  //Multiplying my birth year by 2
	
		System.out.println("My height in centimeters is " + (height * 2.54));  //Converting my height from inches to cms
		
		System.out.println("My height in feet and inches is " + (height / 12) + " feet and " + (height % 12) + " inches");  //Converting my height from inches to feet and inches
 		
		
		
		
	}

}
