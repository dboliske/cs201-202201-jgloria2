package labs.lab1;

import java.util.Scanner;

public class WoodBox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in); //Variable for reading user input
		
		System.out.print("Enter the length of the box in inches:");  //Prompt user for the length of the box in inches
		
		double length = Double.parseDouble(input.nextLine());  //Read in the length and convert to a double
		
		System.out.print("Enter the width of the box in inches:");  //Prompt user for the width of the box in inches
		
		double width = Double.parseDouble(input.nextLine());  //Read in the width and convert to a double
		
		System.out.print("Enter the depth of the box in inches:");  //Prompt user for the depth of the box in inches
		
		double depth = Double.parseDouble(input.nextLine());  //Read in the depth and convert to a double
		
		input.close();  //Closing the scanner
		
		System.out.println("The amount of wood needed to make the box is " + ((length * width * 2) + (length * depth * 2) + (width * depth * 2)) + " square feet");  //Calculating and displaying the amount of wood needed to make the box
		
		
		
		//Test Plan
				//Small Dimensions:   Inputs: (Width = 5, Length = 7, Depth = 9)         Expected Output =    286    Actual Output =   286
				//Medium Dimensions:  Inputs: (Width = 20, Length = 25, Depth = 28)         Expected Output =  3520      Actual Output =  3520 
				//Large Dimensions:   Inputs: (Width = 93, Length = 95, Depth = 100)         Expected Output =  55270      Actual Output =   55270

	}

}
