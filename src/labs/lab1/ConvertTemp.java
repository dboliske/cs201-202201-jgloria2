package labs.lab1;

import java.util.Scanner;

public class ConvertTemp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in); //Variable for reading user input
		
		System.out.print("Enter a temperature in Fahrenheit:");  //Prompt user for a temperature in Fahrenheit
		
		double tempF = Double.parseDouble(input.nextLine());  //Read in the temperature (Fahrenheit) which the user entered and convert to a double
		
		System.out.print("Enter a temperature in Celsius:");  //Prompt user for a temperature in Celsius
		
		double tempC = Double.parseDouble(input.nextLine());  //Read in the temperature (Celsius) which the user entered and convert to a double
		
		input.close();  //Closing the scanner
		
		
		System.out.println("The temperature which you entered in Fahrenheit is " + ((tempF - 32) * (5.0/9)) + " degrees in Celsius");  //Converting the Fahrenheit temperature to Celsius
		
		System.out.println("The temperature which you entered in Celsius is " + ((tempC * (9.0/5)) + 32) + " degrees in Fahrenheit");  //Converting the Celsius temperature to Fahrenheit
		
		
		
		//Test Plan
			//Low (Celsius to F):   Input =  0         Expected Output = 32       Actual Output =    32
			//Low (Fahrenheit to C):   Input =    32       Expected Output =  0      Actual Output =    0
		
			//Medium (Celsius to F):   Input =   15        Expected Output =  59      Actual Output =   59 
			//Medium (Fahrenheit to C):   Input =    59       Expected Output =   15     Actual Output =    15
		
			//High (Celsius to F):   Input =   50        Expected Output =  122      Actual Output =   122 
			//High (Fahrenheit to C):   Input =   122        Expected Output =   50     Actual Output =   50 
		
		
	}

}
