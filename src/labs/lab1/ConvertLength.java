package labs.lab1;

import java.util.Scanner;

public class ConvertLength {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in); //Variable for reading user input
		
		System.out.print("Enter a length in inches:");  //Prompt user for a length in inches
		
		double length = Double.parseDouble(input.nextLine());  //Read in the length and convert to a double
		
		input.close();  //Closing the scanner
		
		System.out.println("The length entered in inches is equivalent to " + (length * 2.54) + " centimeters"); // Converting from inches to cms and displaying the result
		
		
		//Test Plan
			//Example 1:   Input (inches):  5      Expected Output (cms) =  12.7      Actual Output (cms) =   12.7
			//Example 2:   Input (inches):  25     Expected Output (cms) =   63.5     Actual Output (cms) =   63.5
			//Example 3:   Input (inches):  60     Expected Output (cms) =   152.4     Actual Output (cms) =   152.4


	}

}
