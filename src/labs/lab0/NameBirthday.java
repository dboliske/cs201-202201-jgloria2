package labs.lab0;

import java.util.Scanner;

public class NameBirthday {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//System.out.println("My name is Jonathan Gloria and my birthdate is Feb. 23, 2001."); //Print out my Name and Birthdate
		
		//Above is the code to output my name and birthdate using a print statement. Below is the code to output my name and birthdate using scanners
		
		Scanner input = new Scanner(System.in); //Variable for reading user input
		
		System.out.print("Enter your full name:");  //Prompt user for their name
		
		String name = input.nextLine();  //Read in the user's name
		
        System.out.print("Enter first three letters of your birthday month:");  //Prompt user for their birthday month
		
		String month = input.nextLine();  //Read in the user's birthday month
		
        System.out.print("Enter the day of month of your birthday:");  //Prompt user for the day of month of their birthday
		
		String day = input.nextLine();  //Read in the user's day of month of their birthday
		
        System.out.print("Enter the year of your birthday:");  //Prompt user for their birthday year
		
		String year = input.nextLine();  //Read in the user's birthday year
		
		input.close();  //Closing the scanner
		
		System.out.println("My name is " + name + " and my birthdate is " + month + ". " + day + ", " + year + "."); //Printing the name and birthdate
		
	}

}
