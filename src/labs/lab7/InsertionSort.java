package labs.lab7;

public class InsertionSort {

	public static void main(String[] args) {
		
		String[] values = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		values = sort(values);
		
		for(String s : values) {
			
			System.out.print(s + " ");
		}		

	}
	
	
	
	public static String[] sort(String[] values) {
		
		for (int j = 1; j < values.length; j++) {
			
			int i = j;
			
			while(i > 0 && values[i].compareTo(values[i-1]) < 0) {
				
				String temp = values[i];
				values[i] = values[i-1];
				values[i-1] = temp;
				i--;
			}
		}
		
		return values;
		
		
	}

}
