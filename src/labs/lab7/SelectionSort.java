package labs.lab7;

public class SelectionSort {

	public static void main(String[] args) {

		
		double[] values = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		values = sort(values);
		
		for(double d : values) {
			
			System.out.print(d + " ");
		}

	}

	
	public static double[] sort(double[] values) {
		
		for (int i = 0; i < values.length - 1; i++) {
			
			int min = i;
			
			
			for(int j = i + 1; j < values.length; j++) {
				
				if (values[j] < values[min]) {
					
					min = j;

				}
			}
			
			if(min != i) {
				
				double temp = values[i];
				values[i] = values[min];
				values[min] = temp;
				
			}
		}
		
		return values;
	}
}
