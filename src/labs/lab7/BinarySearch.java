package labs.lab7;

import java.util.Scanner;

public class BinarySearch {
	
	
	
	public static int binarySearch(String[] values, String item) {  //utility function
		
		return binarySearch(values, item, values.length - 1, 0);  //When I used "length" I got an error but "length - 1" works
		
	}
	
	
	public static int binarySearch(String[] values, String item, int end, int start) {
		
		
//		  int start = 0;
		  
		  //int end = values.length - 1;
		  
//		  int end = values.length; //Isn't the index at values.length out of bounds? Why isn't it length - 1? 
		
		  int pos = -1; 
		  
//		  boolean found = false;
		 
		
		
		int mid = (start + end) / 2;
		
		if (values[mid].equalsIgnoreCase(item)) {  //base case
			
			pos = mid;
		//	return pos;
			
		}else if (start >= end) {  //base case
			
			return -1;
		}
		
		else if (values[mid].compareToIgnoreCase(item) < 0){  //general case
			
			start = mid + 1;
			pos = binarySearch(values, item, end, start);
			
			
		}else {  //general case
			
			end = mid;
			pos = binarySearch(values, item, end, start);
			
		}								
		
		return pos;
	
	}
	

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		String[] values = {"c", "html", "java", "python", "ruby", "scala"};
		
		//String[] values = {"c"};
		
		System.out.print("Search item: ");
		
		String item = input.nextLine();
		
		int index = binarySearch(values, item);
		
		if (index == -1) {
			
			System.out.println("'" + item + "' not found");
			
		} else {
		
			System.out.println("'" + item + "' is located at index " + index);
		
		}
		input.close();

	}

}
