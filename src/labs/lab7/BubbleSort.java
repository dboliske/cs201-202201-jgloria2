package labs.lab7;

public class BubbleSort {

	public static void main(String[] args) {
		int[] values = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
		values = sort(values);
		
		/*
		 * for (int i=0; i < values.length; i++ ) {
		 * 
		 * System.out.print(values[i] + ""); }
		 */
		
		for(int i : values) {
			
			System.out.print(i + " ");
		}

	}

	
	public static int[] sort(int[] values) {
		
		boolean running = true;
		
		do {
			running = false;
			for (int i = 0; i < values.length-1; i++) {
				
				if (values[i] > values[i+1]) {
					
					running = true;
					
					int temp = values[i+1];
					values[i+1] = values[i];
					values[i] = temp;
				}
			}
			
		} while (running);
				
		return values;		
	}
	
	
	
	
}
