package project;

import java.util.ArrayList;

//Jonathan Gloria, 4/20/2022, Class Role:  This class acts as a shopping cart for the user. When the user chooses to sell an item, all sold items will be added to the "stock", which is an ArrayList of ShelvedItem. Once the user is done selling items, they will be prompted to checkout, which will then delete all of the items in the stock from the list of items in the store 

public class Cart {

	private ArrayList<ShelvedItem> stock;  //Instance variable for the stock which is an ArrayList of ShelvedItem
	
	public Cart() {  //Default Constructor
		
		stock = new ArrayList<ShelvedItem>();
	}
	
	
	public Cart(ArrayList<ShelvedItem> stock) {  //Non default constructor
		
		this.stock = new ArrayList<ShelvedItem>(stock.size());
		for (ShelvedItem item : stock) {
			
			this.stock.add(item);
			
		}
	}
	
	
	public void addToCart(ShelvedItem item) {  //method for adding items to the cart
		
		stock.add(item);
	}
	
	public void removeFromCart(ShelvedItem item) {  //method for removing items from the cart with a Shelved item parameter
		
		stock.remove(item);
	}
	
	public void removeFromCart(int index) {  //method for removing items from the cart with a index as the parameter
		
		stock.remove(index);
	}
	
	public ShelvedItem getItem(int index) {  //getter for the cart
		
		return stock.get(index);
	}
	
	public int cartSize() {  //method for the cart's size
		
		return stock.size();
	}
	

	public String toString() {  //toString method
		
		String contents = "";
		for (int i = 0; i < stock.size(); i++) {
			
			contents += stock.get(i).toString() + "\n";
		}
		
		return contents;
	}
	
	public boolean equals(Object obj) {  //equals method
		
		if (obj == null) {
			
			return false;
			
		} else if (this == obj) {
			
			return true;
			
		} else if (!(obj instanceof Cart)) {
			
			return false;
			
		}
		
		Cart stock = (Cart)obj;		
		
		if (this.cartSize() != stock.cartSize()) {
			
			return false;
		}
		
		for (int i =0; i < this.cartSize(); i++) {
			
			if (this.getItem(i) == null && stock.getItem(i) != null) {
				
				return false;
			} else if (this.getItem(i) != null && stock.getItem(i) == null) {
				
				return false;
			} else if (this.getItem(i) != null && !this.getItem(i).equals(stock.getItem(i))) {
				
				return false;
			}
		}
		
		return true;
		
	}
	
	
	
	public ArrayList<ShelvedItem> checkout(ArrayList<ShelvedItem> items, Cart cart) {  //Checkout method which removes all elements of the cart from another array list of shelved items.
		
		boolean sold = false;
		
		for (int i = 0; i < items.size(); i++) {                        
			
			sold = false;
			
			for (int j = 0; j < cart.cartSize(); j++) {
				
				if (!sold && items.get(i).equals(cart.getItem(j))) {
					
					items.remove(i);
					cart.removeFromCart(j);
					sold = true;
					i--;
			}
			
				
			}
		}
		
		return items;
	}
	
	
	
	
	
	
}
