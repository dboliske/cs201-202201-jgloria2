package project;

// Jonathan Gloria, 4/20/2022, Class Role:  This class represents a shelved item, so any item which is a shelf-stable food or a non-food item will be an instance of this class. Since shelved item's only have a name and price, those are the only instance variables.

public class ShelvedItem {
	
	protected String name;  //Instance variable for the name of the item
	
	private double price;  //Instance variable for the price of the item
	
	public static double priceTolerance = 0.001;  //Static variable for the price tolerance
	
	
	public ShelvedItem() {  //Default Constructor
		
		name = "Pencil";
		price = 1.29;
		
	}
	
	public ShelvedItem(String name, double price) {  //Non-Default COnstructor
		
		this();
		this.name = name;
		setPrice(price);
		
	}

	public String getName() {  //Getter for the name instance variable
		return name;
	}

	public void setName(String name) {  //Setter for the name instance variable
		this.name = name;
	}

	public double getPrice() {  //Getter for the price instance variable
		return price;
	}

	public void setPrice(double price) {  //Setter for the price instance variable
		
		if (price >= 0.0) {
		this.price = price;
		}
	}
	
	public String toString() {  //toString method
		
		return name + "," + price;
	}
	
	public boolean equals(Object obj) {  //equals method
		
		if (obj == null) {
			
			return false;
			
		} else if (this == obj) {
			
			return true;
			
		} else if (!(obj instanceof ShelvedItem)) {
			
			return false;
			
		}
		
		ShelvedItem item = (ShelvedItem)obj;
		
		if (!item.getName().equalsIgnoreCase(this.getName())) {
			
			return false; 
			
		} else if (Math.abs(item.getPrice() - this.price) > priceTolerance) {
			
			return false;
		}
		
		return true;
		
		
	}
	
	
	  public boolean canExpire() {             //Method which returns a boolean to signify if the item can expire. Shelved items can't expire so it returns false
	  
		  return false; 
	  
	  }
	 

	
}
