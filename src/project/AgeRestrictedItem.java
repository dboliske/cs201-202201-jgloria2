package project;

//Jonathan Gloria, 4/20/2022, Class Role:  This class represents an age restricted item, so any item with an age restriction is an instance of this class. This is a subclass of ShelvedItem because it also has a name and price, but an additional instance variable it has is the age restriction (integer).


public class AgeRestrictedItem extends ShelvedItem {

	private int ageRestriction;  //Instance variable for age restriction
	
	public AgeRestrictedItem() {  //Default Constructor
		
		super();
		ageRestriction = 21;
		
	}
	
	public AgeRestrictedItem(String name, double price, int ageRestriction) {  //Non Default Constructor
		
		super(name, price);
		setAgeRestriction(ageRestriction);
	}

	public int getAgeRestriction() {  //Getter for the age restriction
		return ageRestriction;
	}

	public void setAgeRestriction(int ageRestriction) {  //Setter for the age restriction
		
		if (ageRestriction > 0) {
			
		this.ageRestriction = ageRestriction;
		
		} else { 
		  
			  this.ageRestriction = 21; 
		  
		  }
		 
		 
	}
	
	public String toString() {  //toString method
		
		return super.toString() + "," + ageRestriction;
	}
	
	public boolean equals(Object obj) {  //equals method
	
		if (obj == null) {
			
			return false;
			
		} else if (this == obj) {
			
			return true;
			
		} else if (!super.equals(obj)) {
			
			return false;
			
		} else if (!(obj instanceof AgeRestrictedItem)) {
			
			return false;
			
		}
		
		AgeRestrictedItem item = (AgeRestrictedItem)obj;
		
		if (item.getAgeRestriction() != this.ageRestriction) {
			
			return false;
		}
		
		return true;		
		
	}
	
	
	  public boolean canExpire() {             //Overriding the canExpire() method from the ShelvedItems class. This method returns a boolean to signify if the item can expire. Age Restricted Items can't expire so it returns false
		  
	  
		  return false; 
	  
	  }
	 
	
	
	
	
}
