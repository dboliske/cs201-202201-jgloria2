package project;

import java.util.regex.Pattern;

//Jonathan Gloria, 4/20/2022, Class Role:  This class represents a produce item, so any fresh food that will expire if not sold is an instance of this class. This is a subclass of ShelvedItem because it also has a name and price, but an additional instance variable it has is the expiration date (String)

public class ProduceItem extends ShelvedItem {

	private String expirationDate; // Instance variable for the expiration date

	 private static String expirationDatePattern = "([1-9]|[0][1-9]|[1][0-2])(/)"  //Regex pattern for the date

	 + "([1-9]|[0][1-9]|[12][0-9]|[3][01])(/)" + "([0-9]+)";

	public ProduceItem() { // Default constructor

		super();
		expirationDate = "04/29/2022";
	}

	public ProduceItem(String name, double price, String expirationDate) { // Non default constructor
		
		super(name, price);
		setExpirationDate(expirationDate);

	}

	public String getExpirationDate() { // Getter for the expiration date
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) { // Setter for the expiration date

		 if (Pattern.matches(expirationDatePattern, expirationDate)) {

		this.expirationDate = expirationDate;

	 }

		
		  else { 
		  
			  this.expirationDate = "04/29/2022";
		  
		  }
		 

	}

	public String toString() { // toString Method

		return super.toString() + "," + expirationDate;
	}

	public boolean equals(Object obj) { // equals method

		if (obj == null) {

			return false;

		} else if (this == obj) {

			return true;

		} else if (!super.equals(obj)) {

			return false;

		} else if (!(obj instanceof ProduceItem)) {

			return false;

		}

		ProduceItem item = (ProduceItem) obj;

		if (!item.getExpirationDate().equalsIgnoreCase(this.expirationDate)) {

			return false;
		}

		return true;

	}

	
	  public boolean canExpire() {             //Overriding the canExpire() method from the ShelvedItems class. This method returns a boolean to signify if the item can expire. Produce Items can expire so it returns true
	  
		  return true;
	  
	  }
	 

}
