package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

//Jonathan Gloria, 4/23/2022, Class Role:  This is my application class which acts as a general store. It allows a user to read in a file and creates a list of items from that file, then prompts the user to either �add item�, �sell item�, �search item�, �modify item�, "list items", or �exit�. When the user chooses to exit, the current list of items is saved to a file. 

public class GeneralStoreApp {

	public static void main(String[] args) {  //This is my main method where I call all my other static methods

		Scanner input = new Scanner(System.in);

		System.out.print("Enter a filename (with path): ");

		String filename = input.nextLine();

		// String filename = "src/project/stock.csv";
		// String filename = "src/project/TestStock";

		ArrayList<ShelvedItem> items = readFile(filename);

		menu(input, items);

		input.close();

	}

	public static ArrayList<ShelvedItem> readFile(String filename) { // This method is to read in the input file, and create an ArrayList of items from that file

		ArrayList<ShelvedItem> items = new ArrayList<ShelvedItem>();

		try {
			File f = new File(filename);

			Scanner input = new Scanner(f);

			while (input.hasNextLine()) {

				String line = input.nextLine();

				String[] values = line.split(",");

				ShelvedItem alt = null;

				if (values.length < 3) {

					alt = new ShelvedItem(values[0], Double.parseDouble(values[1]));

				} else {

					try {

						alt = new AgeRestrictedItem(values[0], Double.parseDouble(values[1]),
								Integer.parseInt(values[2]));

					} catch (Exception e) {

						alt = new ProduceItem(values[0], Double.parseDouble(values[1]), values[2]);

					}

				}

				items.add(alt); 

			}
	

			input.close();

		} catch (FileNotFoundException e) {

			System.out.println(e.getMessage());

		}


		return items;

	}

	public static ArrayList<ShelvedItem> addItem(ArrayList<ShelvedItem> items, Scanner input) { // This method is for the "add item" menu option, and it prompts the user for the attributes of the item, creates that item, and adds it to the existing ArrayList of items
		

		String expirationDatePattern = "([1-9]|[0][1-9]|[1][0-2])(/)" // Regex pattern for the date 

				+ "([1-9]|[0][1-9]|[12][0-9]|[3][01])(/)" + "([0-9]+)";

		ShelvedItem newItem = null;

		System.out.print("Enter the item's name: ");

		String name = input.nextLine();

		System.out.print("Enter the item's price: ");

		String priceString = input.nextLine();
		double price = 0.0;

		try {

			if (Double.parseDouble(priceString) >= 0.0) {
				
				price = Double.parseDouble(priceString);
			} else {
				
				System.out.println("'" + priceString + "' is not a valid price");
				return items;
			}

		} catch (Exception e) {

			System.out.println("'" + priceString + "' is not a valid price");
			return items;
		}

		System.out.println("1: Shelved Item");
		System.out.println("2: Age Restricted Item");
		System.out.println("3: Produce Item");
		System.out.print("Enter the number corresponding to the type of item: ");

		String type = input.nextLine();

		switch (type) {

		case "1":

			newItem = new ShelvedItem(name, price);

			break;

		case "2":

			System.out.print("Enter the item's age restriction: ");

			String ageString = input.nextLine();
			int age = 0;

			try {

				age = Integer.parseInt(ageString);
				
				if (age >= 0.0) {

					newItem = new AgeRestrictedItem(name, price, age);
				
				} else {
					
					System.out.println("'" + ageString + "' is not a valid age restriction");
					return items;
				}

			} catch (Exception e) {

				System.out.println("'" + ageString + "' is not a valid age restriction");
				return items;
			}


			break;

		case "3":

			System.out.print("Enter the item's expiration date (mm/dd/yyyy): ");

			String date = input.nextLine(); 

			if (Pattern.matches(expirationDatePattern, date)) {

				newItem = new ProduceItem(name, price, date);

			} else {

				System.out.println("'" + date + "' is not  valid date");
				return items;
			}

			break;

		default:

			System.out.println("'" + type + "' is not a valid choice. Enter '1', '2', or '3'.");
			return items;

		}

		items.add(newItem);
		System.out.println("Item has been successfully added");
		return items;
	}

	public static ArrayList<ShelvedItem> menu(Scanner input, ArrayList<ShelvedItem> items) {  //This method is for the menu, and it repeatedly prompts the user for a menu option until the user chooses to "exit"


		boolean running = true;

		do {
			System.out.println("1. Add item");
			System.out.println("2. Sell item");
			System.out.println("3. Modify Item");
			System.out.println("4. Search Item");
			System.out.println("5. List Items");  
			System.out.println("6. Exit");
			System.out.print("Choice: ");

			String choice = input.nextLine();

			switch (choice) {

			case "1": // Add item

				items = addItem(items, input);

				break;

			case "2": // Sell Item

				items = sellItem(input, items);    

				break;

			case "3": // Modify Item

				items = modifyItem(input,items);  

				break;

			case "4": // Search Item

				boolean found = searchItem(input, items);

				if (found) {

					System.out.println("The item is available");

				} else {

					System.out.println("The item is not available");

				}

				break;

			case "5": // List Items

				System.out.println(listOfItems(items, input));

				break;
				
			case "6": // Exit

				running = false;
				
				writeFile(items, input);

				System.out.println("Goodbye!");

				break;

			default:

				System.out.println("'" + choice + "' is not a valid choice");

			}

		} while (running);

		return items;

	}

	public static ArrayList<ShelvedItem> sellItem(Scanner input, ArrayList<ShelvedItem> items) {  //This method is for the "sell item" menu option, and it prompts the user for the items which they want to sell, adds these items to a cart (which is an ArrayList of ShevlvedItem), and then prompts the user to checkout, which will remove the items in the cart from the general store's list of items. 

		Cart stock = new Cart();
		
		for (int i = 0; i < items.size(); i++) {
			
			
			System.out.print("Do you want to sell " + items.get(i) + "? (Enter yes or no): ");
			String choice = input.nextLine();
			switch(choice.toLowerCase()) {
			
			case "yes" :
			case "y":
				
				stock.addToCart(items.get(i));  
				
				break;
				
			case "no":
			case "n":
				
				break;
				
			default:
				
				System.out.println("'" + choice + "' is not a valid choice.");
				return items;
			}
			
		}
		
			System.out.println("Current cart: \n" + stock.toString());  
			
			System.out.print("Do you want to check out? (Enter yes or no)");
			String checkout = input.nextLine().toLowerCase();
			switch(checkout) {
			
			case "yes" :
			case "y":
				
				stock.checkout(items, stock);  
				System.out.println("Successfully checked out.");
				break;
				
			case "no":
			case "n":
				
				System.out.println("Cancelling checkout.");
				break;
				
			default:
				
				System.out.println("'" + checkout + "' is not a valid choice.");
				return items;
			}

		
		return items;
		
	}

	public static boolean searchItem(Scanner input, ArrayList<ShelvedItem> items) {  //This method is for the "search item" menu option. It prompts the user for the item's name, searches through the list of items, and returns a boolean signifying if the item was found.


		System.out.print("Enter the item's name: ");

		String name = input.nextLine();

		int found = search (items, name);
		
		if (found == -1) {

			return false;
		}

		return true;

	}
	 
	
	public static int search(ArrayList<ShelvedItem> items, String name) {  //This method is for the "searchItem()" method, and it searches through the list of items, and returns the index of the item if it is found.
		
		for (int i = 0; i < items.size(); i++) { 
			
			String shelvedItem = items.get(i).toString();
			
			String[] values = shelvedItem.split(",");
			  
			  if (values[0].equals(name)) {
	  
				  return i;
			  
			  }
		  
		}
	  
	  return -1;
	
	}
		

	public static ArrayList<ShelvedItem> modifyItem(Scanner input, ArrayList<ShelvedItem> items) {  //This method is for the "modify item" menu option. It prompts the user for the item's attributes as well as the new value for the attribute that they want to modify, and then replaces all instances of this item in the ArrayList of items with the modified item.

		
		  String expirationDatePattern = "([1-9]|[0][1-9]|[1][0-2])(/)" //Regex pattern for the date 
		  
		  + "([1-9]|[0][1-9]|[12][0-9]|[3][01])(/)" + "([0-9]+)";
		  
		  ShelvedItem oldItem = null;
		 

		  System.out.print("Enter the item's name: ");

		  String name = input.nextLine();

		
		  System.out.print("Enter the item's price: ");
		  
		  String priceString = input.nextLine(); 
		  
		  double price = 0.0;
		  
		  try {
		  
		  price = Double.parseDouble(priceString);
		  
		  } catch(Exception e) {
		  
		  System.out.println("'" + priceString + "' is not a valid price"); 
		  return items; 
		  
		  }
		  
		  
		  System.out.println("1: Shelved Item");
		  System.out.println("2: Age Restricted Item");
		  System.out.println("3: Produce Item");
		  System.out.print("Enter the number corresponding to the type of item: ");
		  
		  String type = input.nextLine();
		  
		  int age = 0;  //Initializing the variables outside the switch case
		  String date = null;  //Initializing the variables outside the switch case
		  
		  switch (type) {
		  
		  case "1" :
		  
		  oldItem = new ShelvedItem(name, price);
		  
		  break;
		  
		  case "2" :
		  
		  System.out.print("Enter the item's age restriction: ");
		  
		  String ageString = input.nextLine(); 
		  
		  try {
		  
		  age = Integer.parseInt(ageString);
		  
		  oldItem = new AgeRestrictedItem(name, price, age);
		  
		  } catch(Exception e) {
		  
		  System.out.println("'" + ageString + "' is not a valid age restriction");
		  return items; 
		  }
		  
		  break;
		  
		  case "3" :
		  
		  System.out.print("Enter the item's expiration date (mm/dd/yyyy): ");

		  date = input.nextLine(); 	
		  
		  if (Pattern.matches(expirationDatePattern, date)) {
		  
		  oldItem = new ProduceItem(name, price, date);
		  
		  } else {
		  
		  System.out.println("'" + date + "' is not  valid date"); 
		  return items; 
		  
		  }
		  
		  break;
		  
		  default :
		  
		  System.out.println("'" + type + "' is not a valid choice. Enter '1', '2', or '3'."); 
		  return items;
		  
		  }		
		  
		  
		  ShelvedItem newItem = null;
		
		  System.out.println("1: Name");
		  System.out.println("2: Price");
		  System.out.println("3: Age Restriction");
		  System.out.println("4: Expiration Date");
		  System.out.print("Enter the number corresponding to the attribute you want to modify: ");
		
		  String attribute = input.nextLine();
		  switch (attribute) {
		  
		  case "1" :  //Name
		  
			  System.out.println("Enter the new name: ");
			  String newName = input.nextLine();
		
			  if (type.equals("1")) {
				  
				  newItem = new ShelvedItem(newName, price);
			  } else if (type.equals("2")) {
				  
				  newItem = new AgeRestrictedItem(newName, price, age);
				  
			  } else if (type.equals("3")) {
				  
				  newItem = new ProduceItem(newName, price, date);
			  }
			  
		  
		  break;
		  
		  case "2" :  //Price
		  
		  System.out.print("Enter the item's new price: ");
		  
		  String newPriceString = input.nextLine(); 
		  
		  double newPrice = 0.0;
		  
		  try {
		  
		  newPrice = Double.parseDouble(newPriceString);
		  
		  if (type.equals("1")) {
			  
			  newItem = new ShelvedItem(name, newPrice);
		  } else if (type.equals("2")) {
			  
			  newItem = new AgeRestrictedItem(name, newPrice, age);
			  
		  } else if (type.equals("3")) {
			  
			  newItem = new ProduceItem(name, newPrice, date);
		  }
		  
		  
		  } catch(Exception e) {
		  
		  System.out.println("'" + newPriceString + "' is not a valid price");
		  return items; 
		  }
		  
		  break;
		  
		  case "3" :  //Age Restriction
			  
			 if (!type.equals("2")) {
				 
				 System.out.println("This type of item doesn't have an age restriction");
				 return items;
			 }
		  
			 System.out.print("Enter the item's new age restriction: ");
			  
			  String newAgeString = input.nextLine(); 
			  
			  int newAge = 0;
			  
			  try {
			  
			  newAge = Integer.parseInt(newAgeString);
			  
			  newItem = new AgeRestrictedItem(name, price, newAge);
			  
			  } catch(Exception e) {
			  
			  System.out.println("'" + newAgeString + "' is not a valid age restriction");
			  return items; 
			  }
		  
		  break;
		  
		  case "4" :  //Expiration Date
			  
			 if (!type.equals("3")) {
				 
				 System.out.println("This type of item doesn't have an age expiration date");
				 return items;
			 }
		  
		  System.out.print("Enter the item's new expiration date (mm/dd/yyyy): ");
		  
		  String newDate = input.nextLine(); //Use regex to ensure a valid date is entered
		  
		  if (Pattern.matches(expirationDatePattern, newDate)) {
		  
		  newItem = new ProduceItem(name, price, newDate);
		  
		  } else {
		  
		  System.out.println("'" + newDate + "' is not  valid date"); 
		  return items; 
		  
		  }
		  
		  break;		  
		  
		  default :
		  
		  System.out.println("'" + type + "' is not a valid choice. Enter '1', '2', or '3'."); 
		  return items;
		  
		  }			
		  
		  ArrayList<ShelvedItem> oldStock = new ArrayList<ShelvedItem>(items.size());  //Making a backup of the list of old items
		  
		  oldStock.addAll(items);                                       //Copying item's elements to the backup
		  
		  
			for (int i = 0; i < items.size(); i++) {   //Setting the old item to the new item

				if (items.get(i).equals(oldItem)) {
					
					items.set(i, newItem);
				}

			}
			
			if (items.equals(oldStock)) {   //This is in case the user tries to modify an item that isn't in the items ArrayList
				
				System.out.println("The item you attempted to modify isn't available");
				return items;
			}
			
			System.out.println("The item has been succesfully modified.");
			return items;
	}

	public static void writeFile(ArrayList<ShelvedItem> items, Scanner input) {  //This method is for the "exit" menu option, and it saves the current ArrayList of items to a file specified by the user
		
		System.out.print("Enter a filename (with path): ");
		String filename = input.nextLine();
		
		try {
			
			FileWriter writer = new FileWriter(filename);
			
			for (int i = 0; i<items.size(); i++) {
				
				writer.write(items.get(i).toString() + "\n");
				writer.flush();
			}
			
			writer.close();
			System.out.println("The file has been succesfully saved.");
			
		} catch (Exception e) {
			
			System.out.println("An error occured when saving the file.");
		}

	}
	
	
	public static String listOfItems(ArrayList<ShelvedItem> items, Scanner input) {  //This method is for the "list items" menu option. It prompts the user for the type of items they want listed, and then lists those items
		
		Cart list = new Cart();
		
		System.out.println("1. List of Shelved Items");
		System.out.println("2. List of Produce Items");
		System.out.println("3. List of Age Restricted Items");
		System.out.println("4. List of All Items");
		System.out.print("Choice: ");
		
		String choice = input.nextLine();
		switch(choice) {
			
			case "1" :  //List of Shelved Items
				
				for (int i = 0; i < items.size(); i++) {  

					if (!items.get(i).canExpire() && items.get(i) instanceof ShelvedItem && !(items.get(i) instanceof AgeRestrictedItem)) { 
						
						list.addToCart(items.get(i));
					}

				}
				
				break;
				
			case "2" :  //List of Produce Items
				
				for (int i = 0; i < items.size(); i++) {  

					if (items.get(i).canExpire()) { 
						
						list.addToCart(items.get(i));
					}

				}
				
				break;
				
			case "3" :  //List of Age Restricted Items
				
				for (int i = 0; i < items.size(); i++) {  

					if (!items.get(i).canExpire() && items.get(i) instanceof AgeRestrictedItem) {  
						
						list.addToCart(items.get(i));
					}

				}
				
				break;
				
			case "4" :  //List of All Items
				
				for (int i = 0; i < items.size(); i++) {  

					list.addToCart(items.get(i));

				}
				
				break;
				
			default:
				
				return "'" + choice +  "' is not a valid choice.";
		
		}
		
		return "List: \n" + list.toString();
	}

}
